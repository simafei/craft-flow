package com.simafei.flow;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.simafei.flow.web.common.BasePO;

import java.sql.Types;

/**
 * @author fengpengju
 */
public class Generator {

    public static void main(String[] args) {
        // 使用 FastAutoGenerator 快速配置代码生成器
        FastAutoGenerator.create("jdbc:mysql://t4xwi1l5822m8.oceanbase.aliyuncs.com/test?serverTimezone=GMT%2B8", "micro_admin", "micro@testDB")
                .globalConfig(builder -> {
                    builder.author("fengpengju") // 设置作者
                            .outputDir("flow-web/src/main/java"); // 输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.simafei.flow.web") // 设置父包名
                            .entity("entity") // 设置实体类包名
                            .mapper("dao"); // 设置 Mapper 接口包名
                            //.service("service") // 设置 Service 接口包名
                            //.serviceImpl("service.impl") // 设置 Service 实现类包名
                            //.xml("mappers"); // 设置 Mapper XML 文件包名

                })
                .dataSourceConfig(builder -> builder.typeConvertHandler((globalConfig, typeRegistry, metaInfo) -> {
                    int typeCode = metaInfo.getJdbcType().TYPE_CODE;
                    if (typeCode == Types.SMALLINT || typeCode == Types.TINYINT) {
                        // 自定义类型转换
                        return DbColumnType.INTEGER;
                    }
                    if (typeCode == Types.BIT) {
                        return DbColumnType.BOOLEAN;
                    }
                    return typeRegistry.getColumnType(metaInfo);

                }))
                .strategyConfig(builder -> {
                    builder//.addInclude("data_source") // 设置需要生成的表名
                            .entityBuilder()
                            .enableLombok() // 启用 Lombok
                            .enableTableFieldAnnotation() // 启用字段注解
                            .controllerBuilder()
                            .enableRestStyle()
                            .entityBuilder()
                            .superClass(BasePO.class)
                            .enableFileOverride()
                            .addSuperEntityColumns("id", "create_time", "update_time", "creator", "updater")
                            .addTableFills(new Column("create_time", FieldFill.INSERT))
                            .addTableFills(new Column("update_time", FieldFill.INSERT_UPDATE))
                            .formatFileName("%sPO");
                })
                .templateEngine(new FreemarkerTemplateEngine())
                .execute(); // 执行生成
    }
}
