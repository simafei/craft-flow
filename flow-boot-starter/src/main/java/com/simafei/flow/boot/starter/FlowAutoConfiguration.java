package com.simafei.flow.boot.starter;

import com.simafei.flow.core.FlowFactory;
import com.simafei.flow.core.api.impl.ApiExecutor;
import com.simafei.flow.core.data.DataManager;
import com.simafei.flow.core.data.ds.FlowDataSource;
import com.simafei.flow.core.data.impl.SqlDataManager;
import com.simafei.flow.core.rule.RuleExecutor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author fengpengju
 */
@Configuration
public class FlowAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public DataManager dataManager(DataSource dataSource) {
        FlowDataSource flowDataSource = new FlowDataSource();
        flowDataSource.add("MASTER", dataSource);
        return new SqlDataManager(flowDataSource);
    }

    @Bean
    @ConditionalOnMissingBean
    public ApiExecutor apiExecutor() {
        return new ApiExecutor();
    }

    @Bean
    @ConditionalOnMissingBean
    public RuleExecutor ruleExecutor() {
        return new RuleExecutor();
    }

    @Bean
    @ConditionalOnMissingBean
    public FlowFactory flowFactory(DataManager dataManager, RuleExecutor ruleExecutor, ApiExecutor apiExecutor) {
        return new FlowFactory(ruleExecutor, apiExecutor, dataManager);
    }
}
