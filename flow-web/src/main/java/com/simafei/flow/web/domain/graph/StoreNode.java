package com.simafei.flow.web.domain.graph;

import com.simafei.flow.core.data.StoreSpec;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author fengpengju
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "存储节点")
public class StoreNode extends GraphNode<StoreSpec> {

    @Schema(description = "数据修改配置")
    private StoreSpec storeSpec;

    @Override
    public StoreSpec extend() {
        return storeSpec;
    }
}
