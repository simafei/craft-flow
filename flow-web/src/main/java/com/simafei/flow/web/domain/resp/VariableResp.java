package com.simafei.flow.web.domain.resp;

import com.simafei.flow.core.common.FieldScope;
import com.simafei.flow.core.common.FieldType;
import com.simafei.flow.core.common.Option;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
*
* 对象字段表
* @author Generator
* @since 2024-01-02
*/
@Data
@Schema(description="对象字段表-传输对象")
public class VariableResp implements Serializable {

    @Schema(description = "主键ID")
    private Long id;

    @Schema(description = "流程id")
    private Long flowId;

    @Schema(description = "字段名称")
    private String varName;

    @Schema(description = "字段中文名")
    private String varLabel;

    @Schema(description = "字段类型 0: 字符, 1: 整数, 2:浮点数")
    private FieldType varType;

    @Schema(description = "数据作用域 0: 请求，1: 赋值")
    private FieldScope scope;

    @Schema(description = "初始值")
    private String initValue;

    @Schema(description = "枚举选项")
    private List<Option<String, String>> options;
}

