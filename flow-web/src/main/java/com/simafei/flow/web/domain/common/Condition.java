package com.simafei.flow.web.domain.common;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author fengpengju
 */
@Schema(description = "执行条件", subTypes = {Criteria.class, Criterion.class})
public interface Condition {
}
