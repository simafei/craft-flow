package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("node_result")
public class NodeResultPO extends BasePO {


    /**
     * 执行请求id
     */
    @TableField("exec_id")
    private String execId;

    /**
     * 执行规则id
     */
    @TableField("flow_id")
    private String flowId;

    /**
     * 执行节点Id
     */
    @TableField("node_id")
    private String nodeId;

    /**
     * 上一执行节点
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 节点类型
     */
    @TableField("node_type")
    private String nodeType;

    /**
     * 异常信息
     */
    @TableField("error_msg")
    private String errorMsg;

    /**
     * 执行参数
     */
    @TableField("exec_params")
    private String execParams;

    /**
     * 执行结果
     */
    @TableField("exec_result")
    private String execResult;

    /**
     * 输出变量列表
     */
    @TableField("output_vars")
    private String outputVars;

    /**
     * 是否成功
     */
    @TableField("success")
    private Boolean success;

    /**
     * 开始时间
     */
    @TableField("start_time")
    private Long startTime;

    /**
     * 结束时间
     */
    @TableField("end_time")
    private Long endTime;

    /**
     * 执行状态，PASS,REJECT,WAITTING,FINISHED,FAILD

     */
    @TableField("exec_status")
    private String execStatus;
}
