package com.simafei.flow.web.domain.graph;

import com.simafei.flow.core.common.Criteria;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author fengpengju
 */
@Data
public class GraphEdge {

    @Schema(description = "边Id")
    private String id;

    @Schema(description = "from节点Id")
    private String fromId;

    @Schema(description = "to节点Id")
    private String toId;

    @Schema(description = "执行条件", implementation = com.simafei.flow.web.domain.common.Criteria.class)
    private Criteria criteria;

}
