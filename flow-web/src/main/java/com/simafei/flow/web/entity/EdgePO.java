package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 决策流节点连线
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("edge")
public class EdgePO extends BasePO {

    private static final long serialVersionUID = 1L;

    /**
     * 决策流Id
     */
    @TableField("flow_id")
    private Long flowId;

    /**
     * 连线Id
     */
    @TableField("edge_id")
    private String edgeId;

    /**
     * from节点Id
     */
    @TableField("from_node_id")
    private String fromNodeId;

    /**
     * to节点Id
     */
    @TableField("to_node_id")
    private String toNodeId;

    /**
     * 执行条件
     */
    @TableField("criteria")
    private String criteria;
}
