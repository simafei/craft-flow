package com.simafei.flow.web.domain.resp;

import com.simafei.flow.core.common.ExecStatus;
import com.simafei.flow.core.common.FlowStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
*
* 
* @author Generator
* @since 2024-01-15
*/
@Data
@Schema(description="流程执行结果-传输对象")
public class FlowResultResp implements Serializable {

    @Schema(description = "主键ID")
    private Long id;

    @Schema(description = "执行请求id")
    private String execId;

    @Schema(description = "执行规则id")
    private String flowId;

    @Schema(description = "执行规则名称")
    private String flowName;

    @Schema(description = "执行参数")
    private String execParam;

    @Schema(description = "执行结果")
    private String execResult;

    @Schema(description = "执行状态")
    private ExecStatus execStatus;

    @Schema(description = "异常信息")
    private String errorMsg;

    @Schema(description = "执行时间")
    private Long spendTime;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "创建人")
    private String creator;

    @Schema(description = "更新人")
    private String updater;

    @Schema(description = "测试模式")
    private Boolean testMode;

    @Schema(description = "执行模式，0: 自动决策，1: 人工干预")
    private Integer execMode;

    @Schema(description = "决策结果，0: 待定，1: 通过，-1: 拒绝")
    private FlowStatus flowStatus;

    @Schema(description = "版本号")
    private String versionNo;

    @Schema(description = "权重")
    private Integer weight;
}

