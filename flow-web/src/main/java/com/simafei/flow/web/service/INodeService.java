package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.web.entity.NodePO;

/**
 * <p>
 * 决策流节点 服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface INodeService extends IService<NodePO> {

}
