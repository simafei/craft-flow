package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 场景分类
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("category")
public class CategoryPO extends BasePO {

    /**
     * 分类名称
     */
    @TableField("name")
    private String name;

    /**
     * 图标名称
     */
    @TableField("icon")
    private String icon;
}
