package com.simafei.flow.web.controller;

import com.simafei.flow.web.common.CommonResult;
import com.simafei.flow.web.domain.req.FlowResultReq;
import com.simafei.flow.web.domain.resp.FlowResultResp;
import com.simafei.flow.web.domain.resp.FrDetailResp;
import com.simafei.flow.web.domain.resp.PageResult;
import com.simafei.flow.web.service.IFlowResultService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
@Tag(name = "决策流执行结果")
@RestController
@RequestMapping("/flow-result")
@RequiredArgsConstructor
public class FlowResultController {

    private final IFlowResultService flowResultService;

    @Operation(summary = "查询决策流执行结果")
    @GetMapping("/exec-result/get")
    public CommonResult<FlowResultResp> get(@RequestParam String execId) {
        return CommonResult.success(flowResultService.getByExecId(execId));
    }

    @Operation(summary = "分页查询决策流执行结果")
    @PostMapping("/exec-result/page")
    public CommonResult<PageResult<FlowResultResp>> query(@Validated @RequestBody FlowResultReq req) {
        return CommonResult.success(flowResultService.queryPage(req));
    }

    @Operation(summary = "查询决策流执行结果详情")
    @GetMapping("/exec-result/detail")
    public CommonResult<FrDetailResp> query(@RequestParam String execId) {
        return CommonResult.success(flowResultService.detail(execId));
    }
}
