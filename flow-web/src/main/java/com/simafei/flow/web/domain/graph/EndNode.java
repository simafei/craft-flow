package com.simafei.flow.web.domain.graph;

import com.simafei.flow.web.domain.req.VariableReq;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author fengpengju
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "结束节点")
public class EndNode extends GraphNode<List<VariableReq>> {

    @Schema(description = "输出变量")
    private List<VariableReq> outVars;

    @Override
    public List<VariableReq> extend() {
        return outVars;
    }
}
