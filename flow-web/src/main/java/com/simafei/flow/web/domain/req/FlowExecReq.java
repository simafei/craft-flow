package com.simafei.flow.web.domain.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Map;

/**
 * @author fengpengju
 */
@Data
public class FlowExecReq {

    @Schema(description = "决策流ID")
    private Long flowId;

    @Schema(description = "决策流参数")
    private Map<String, Object> param;
}
