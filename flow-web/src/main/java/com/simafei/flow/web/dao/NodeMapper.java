package com.simafei.flow.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simafei.flow.web.entity.NodePO;

/**
 * <p>
 * 决策流节点 Mapper 接口
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface NodeMapper extends BaseMapper<NodePO> {

}
