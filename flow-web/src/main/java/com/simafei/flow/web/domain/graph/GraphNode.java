package com.simafei.flow.web.domain.graph;

import com.simafei.flow.core.common.NodeType;
import com.simafei.flow.web.domain.req.VariableReq;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
@Data
@Schema(description = "节点", subTypes = {AggNode.class, ApiNode.class,
        DataNode.class, EndNode.class, RuleNode.class, StoreNode.class})
public class GraphNode<T> {

    /**
     * 节点Id
     */
    @Schema(description = "节点Id")
    @NotNull(message = "节点Id不能为空")
    private String id;

    /**
     * 节点名称
     */
    @Schema(description = "节点名称")
    @NotNull(message = "节点名称不能为空")
    private String name;

    /**
     * 是否阻塞
     */
    @Schema(description = "是否阻塞")
    private boolean blocked;

    /**
     * 透传字段
     */
    @Schema(description = "透传字段")
    private Map<String, Object> payload;

    /**
     * 节点类型
     */
    @Schema(description = "节点类型")
    @NotNull(message = "节点类型不能为空")
    private NodeType nodeType;

    @Schema(description = "节点变量列表")
    private List<VariableReq> variables;

    /**
     * 扩展字段
     *
     * @return T
     */
    public T extend() {
        return null;
    }
}
