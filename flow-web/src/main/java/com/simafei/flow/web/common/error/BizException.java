package com.simafei.flow.web.common.error;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务逻辑异常 Exception
 * @author fengpengju
 */
@Data
@EqualsAndHashCode(callSuper = true)
public final class BizException extends RuntimeException {

    /**
     * 业务错误码
     *
     */
    private Integer code;
    /**
     * 错误提示
     */
    private String message;

    /**
     * 空构造方法，避免反序列化问题
     */
    public BizException() {
    }

    public BizException(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.message = errorCode.getMsg();
    }

    public BizException(ErrorCode errorCode, Object... args) {
        this.code = errorCode.getCode();
        this.message = StrUtil.format(errorCode.getMsg(), args);
    }

    public BizException(Integer code, String zhCnMessage) {
        this.code = code;
        this.message = zhCnMessage;
    }

    public BizException(Integer code, String zhCnMessage, Object... args) {
        this.code = code;
        this.message = StrUtil.format(zhCnMessage, args);
    }

    public Integer getCode() {
        return code;
    }

    public BizException setCode(Integer code) {
        this.code = code;
        return this;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
