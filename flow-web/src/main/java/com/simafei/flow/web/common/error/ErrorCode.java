package com.simafei.flow.web.common.error;

import lombok.Data;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

/**
 * 错误码对象
 * <p>
 * @author fengpengju
 */
@Data
public class ErrorCode {

    /**
     * 错误码
     */
    private final Integer code;
    /**
     * 错误提示
     */
    private final String msg;

    public ErrorCode(Integer code, String zhCnMessage) {
        this.code = code;
        this.msg = zhCnMessage;
    }

    public ErrorCode(Integer code, String zhCnMessage, String enMessage) {
        this.code = code;
        this.msg = zhCnMessage;
    }

    public String getMsg() {
        Locale locale = LocaleContextHolder.getLocale();
        return this.msg;
    }
}
