package com.simafei.flow.web.domain.req;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author fengpengju
 */
@Data
public class GraphSaveReq {

    @Schema(description = "流程id")
    private Long flowId;

    @Schema(description = "流程图")
    @NotNull(message = "流程图不能为空")
    private String graph;
}
