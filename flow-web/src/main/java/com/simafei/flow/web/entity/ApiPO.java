package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 算法模型表
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("api")
public class ApiPO extends BasePO {

    /**
     * API名称
     */
    @TableField("api_name")
    private String apiName;

    /**
     * API描述
     */
    @TableField("api_desc")
    private String apiDesc;

    /**
     * API状态
     */
    @TableField("api_status")
    private Integer apiStatus;

    /**
     * 0: 自定义参数，1:依赖前一节点
     */
    @TableField("var_mode")
    private Integer varMode;

    /**
     * API使用的模板
     */
    @TableField("template")
    private String template;

    @TableField("params_json")
    private String paramsJson;

    /**
     * API调用地址
     */
    @TableField("http_url")
    private String httpUrl;

    @TableField("http_method")
    private String httpMethod;

    /**
     * 组装请求参数和Header的脚本
     */
    @TableField("serialize_script")
    private String serializeScript;

    /**
     * 序列化类型，0:json,  1:xml
     */
    @TableField("serialize_type")
    private Integer serializeType;

    /**
     * 请求参数
     */
    @TableField("req_variables")
    private String reqVariables;

    /**
     * 返回参数
     */
    @TableField("resp_variables")
    private String respVariables;
}
