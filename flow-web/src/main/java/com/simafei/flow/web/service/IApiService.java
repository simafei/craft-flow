package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.web.domain.req.ApiReq;
import com.simafei.flow.web.domain.req.PageParam;
import com.simafei.flow.web.domain.req.VariableReq;
import com.simafei.flow.web.domain.resp.ApiResp;
import com.simafei.flow.web.domain.resp.PageResult;
import com.simafei.flow.web.entity.ApiPO;

import java.util.List;

/**
 * <p>
 * 算法模型表 服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface IApiService extends IService<ApiPO> {

    /**
     * 获取详情
     *
     * @param id 主键
     * @return 详情
     */
    ApiResp get(Long id);

    /**
     * 新增
     *
     * @param req 请求
     * @return 主键
     */
    Long add(ApiReq req);

    /**
     * 修改
     *
     * @param id  主键
     * @param req 请求
     * @return 是否成功
     */
    boolean update(Long id, ApiReq req);

    /**
     * 设置响应变量
     *
     * @param apiId      模型id
     * @param variables  变量列表
     * @return 是否成功
     */
    boolean setRespVariables(Long apiId, List<VariableReq> variables);

    /**
     * 查询
     *
     * @param apiName 名称
     * @return 列表
     */
    List<ApiResp> queryByName(String apiName);

    /**
     * 分页查询
     *
     * @param param   分页参数
     * @param enabled 是否启用
     * @return 分页结果
     */
    PageResult<ApiResp> queryPage(PageParam param, boolean enabled);

    /**
     * 分页查询
     *
     * @param param   分页参数
     * @return 分页结果
     */
    PageResult<ApiResp> queryPage(PageParam param);
}
