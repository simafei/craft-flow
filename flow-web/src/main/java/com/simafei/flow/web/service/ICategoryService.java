package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.web.domain.req.CategoryReq;
import com.simafei.flow.web.entity.CategoryPO;

/**
 * <p>
 * 场景分类 服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface ICategoryService extends IService<CategoryPO> {

    /**
     * 新增场景分类
     * @param req 场景分类请求
     * @return 分类id
     */
    Long add(CategoryReq req);

    /**
     * 场景分类重命名
     * @param id 分类id
     * @return 是否成功
     */
    boolean delete(Long id);

    /**
     * 场景分类重命名
     * @param id 分类id
     * @param name 分类名称
     * @param icon 分类图标
     * @return 是否成功
     */
    boolean rename(Long id, String name, String icon);
}
