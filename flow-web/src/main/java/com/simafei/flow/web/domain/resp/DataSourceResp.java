package com.simafei.flow.web.domain.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-15
 */
@Data
public class DataSourceResp implements Serializable {

    @Schema(description = "主键ID")
    private Long id;

    @Schema(description = "数据源名称")
    private String dsName;

    @Schema(description = "数据库名称")
    private String dbName;

    @Schema(description = "连接url")
    private String url;

    @Schema(description = "驱动类")
    private String driverClass;

    @Schema(description = "用户名")
    private String username;
}
