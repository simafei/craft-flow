package com.simafei.flow.web.domain.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * 决策流执行详情响应
 * @author fengpengju
 */
@Data
public class FrDetailResp {

    @Schema(description = "节点执行明细")
    private List<NodeResultResp> nodeResults;

    @Schema(description = "连线执行明细")
    private List<EdgeResultResp> edgeResults;
}
