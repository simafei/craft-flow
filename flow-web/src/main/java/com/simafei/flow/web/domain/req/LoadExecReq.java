package com.simafei.flow.web.domain.req;

import com.simafei.flow.core.common.Criteria;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Map;
import java.util.Set;

/**
 * @author fengpengju
 */
@Data
public class LoadExecReq {

    @Schema(description = "数据源")
    private String dataSource;

    /**
     * 数据表名
     */
    @Schema(description = "数据表名")
    private String tableName;

    /**
     * 查询列
     */
    @Schema(description = "查询列")
    private Set<String> columns;

    /**
     * 查询条件
     */
    @Schema(description = "查询条件")
    private Criteria criteria;

    @Schema(description = "参数")
    private Map<String, Object> params;
}
