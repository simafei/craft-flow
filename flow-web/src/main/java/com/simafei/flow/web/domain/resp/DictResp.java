package com.simafei.flow.web.domain.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author fengpengju
 */
@Data
@Schema(description = "字典")
public class DictResp {

    @Schema(description = "字典编码")
    private Object dictCode;

    @Schema(description = "字典名称")
    private String dictName;

    @Schema(description = "是否启用")
    private Boolean enable;
}
