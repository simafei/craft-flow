package com.simafei.flow.web.transfer;

import com.simafei.flow.web.domain.req.DataSourceReq;
import com.simafei.flow.web.domain.resp.DataSourceResp;
import com.simafei.flow.web.entity.DataSourcePO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface DataSourceTransfer {

    DataSourceTransfer INSTANCE = Mappers.getMapper(DataSourceTransfer.class);

    /**
     * entity转dto
     * @param dataSource 数据源
     * @return 规则dto
     */
    DataSourceResp toResp(DataSourcePO dataSource);

    /**
     * entity转dto
     * @param dataSources 数据源
     * @return 规则dto
     */
    List<DataSourceResp> toResp(List<DataSourcePO> dataSources);

    /**
     * entity转PO
     * @param dataSourceReq 数据源请求
     * @return entity
     */
    DataSourcePO toPo(DataSourceReq dataSourceReq);
}
