package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.web.domain.resp.CategoryResp;
import com.simafei.flow.web.domain.resp.FlowResultResp;
import com.simafei.flow.web.entity.GroupPO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface IGroupService extends IService<GroupPO> {

    /**
     * 查询分类流程
     * @return 分类流程
     */
    List<CategoryResp> listWithCategory();

    /**
     * 执行规则
     * @param groupId 分组id
     * @param param 参数
     * @return 执行结果
     */
    FlowResultResp execute(Long groupId, Map<String, Object> param);

    /**
     * 异步执行规则
     * @param groupId 分组id
     * @param param 参数
     * @return 执行结果
     */
    String executeAsync(Long groupId, Map<String, Object> param);

    /**
     * 重命名
     * @param groupId 分组id
     * @param groupName 分组名称
     * @param groupDesc 分组描述
     * @return 是否成功
     */
    boolean rename(Long groupId, String groupName, String groupDesc);
}
