package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("flow_result")
public class FlowResultPO extends BasePO {


    /**
     * 执行请求id
     */
    @TableField("exec_id")
    private String execId;

    /**
     * 执行规则id
     */
    @TableField("flow_id")
    private String flowId;

    /**
     * 批次id
     */
    @TableField("batch_id")
    private String batchId;

    /**
     * 执行参数
     */
    @TableField("exec_param")
    private String execParam;

    /**
     * 执行结果
     */
    @TableField("exec_result")
    private String execResult;

    /**
     * 执行状态
     */
    @TableField("exec_status")
    private String execStatus;

    /**
     * 异常信息
     */
    @TableField("error_msg")
    private String errorMsg;

    /**
     * 执行时间
     */
    @TableField("spend_time")
    private Long spendTime;

    /**
     * 是否是测试模式
     */
    @TableField("test_mode")
    private Boolean testMode;

    /**
     * 执行模式，0：自动决策，1：人工决策
     */
    @TableField("exec_mode")
    private Integer execMode;

    /**
     * 最终流程状态
     */
    @TableField("flow_status")
    private String flowStatus;
}
