package com.simafei.flow.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.simafei.flow.web.dao.NodeMapper;
import com.simafei.flow.web.entity.NodePO;
import com.simafei.flow.web.service.INodeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 决策流节点 服务实现类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
@Service
public class NodeServiceImpl extends ServiceImpl<NodeMapper, NodePO> implements INodeService {

}
