package com.simafei.flow.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.simafei.flow.web.dao.VariableMapper;
import com.simafei.flow.web.entity.VariablePO;
import com.simafei.flow.web.service.IVariableService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 对象字段表 服务实现类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
@Service
public class VariableServiceImpl extends ServiceImpl<VariableMapper, VariablePO> implements IVariableService {

}
