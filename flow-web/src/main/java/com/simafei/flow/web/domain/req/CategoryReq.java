package com.simafei.flow.web.domain.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
*
* 场景分类
* @author Generator
* @since 2023-12-19
*/
@Data
@Schema(description="场景分类-传输对象")
public class CategoryReq implements Serializable {


    @Schema(description = "分类名称")
    private String name;

    @Schema(description = "icon")
    private String icon;
}

