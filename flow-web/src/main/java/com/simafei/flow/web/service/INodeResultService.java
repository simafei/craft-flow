package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.web.entity.NodeResultPO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface INodeResultService extends IService<NodeResultPO> {

    /**
     * 替换
     * @param po po
     */
    void replace(NodeResultPO po);
}
