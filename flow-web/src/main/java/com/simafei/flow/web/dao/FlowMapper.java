package com.simafei.flow.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simafei.flow.web.entity.FlowPO;

/**
 * <p>
 * 决策流 Mapper 接口
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface FlowMapper extends BaseMapper<FlowPO> {

}
