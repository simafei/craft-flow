package com.simafei.flow.web.common;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.simafei.flow.web.domain.resp.PageResult;

import java.util.List;
import java.util.function.Function;

/**
 * @author fengpengju
 */
public class PageUtils {

    public static <R, T> PageResult<R> buildPageResult(Page<T> page, List<R> recodes) {
        PageResult<R> pageResult = new PageResult<>(recodes, page.getTotal());
        pageResult.setCurrentPage(page.getCurrent());
        pageResult.setPageSize(page.getSize());
        pageResult.setTotalPages(page.getPages());
        return pageResult;
    }

    public static <T, M> PageResult<T> buildPageResult(Page<M> page, Function<List<M>, List<T>> function) {
        List<T> apply = function.apply(page.getRecords());
        PageResult<T> pageResult = new PageResult<>(apply, page.getTotal());
        pageResult.setCurrentPage(page.getCurrent());
        pageResult.setPageSize(page.getSize());
        pageResult.setTotalPages(page.getPages());
        return pageResult;
    }
}
