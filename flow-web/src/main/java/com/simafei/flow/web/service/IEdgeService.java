package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.web.entity.EdgePO;

/**
 * <p>
 * 决策流节点连线 服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface IEdgeService extends IService<EdgePO> {

}
