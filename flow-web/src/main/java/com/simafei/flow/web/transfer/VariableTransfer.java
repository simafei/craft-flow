package com.simafei.flow.web.transfer;

import com.simafei.flow.core.common.Variable;
import com.simafei.flow.web.domain.req.VariableReq;
import com.simafei.flow.web.domain.resp.VariableResp;
import com.simafei.flow.web.entity.VariablePO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface VariableTransfer {

    VariableTransfer INSTANCE = Mappers.getMapper(VariableTransfer.class);

    /**
     * entity转dto
     * @param var 模型
     * @return 规则dto
     */
    VariableResp toResp(VariablePO var);

    /**
     * entity转dto
     * @param vars 变量列表
     * @return 规则dto
     */
    List<VariableResp> toResp(List<VariablePO> vars);

    /**
     * entity转dto
     * @param vars 变量列表
     * @return 规则dto
     */
    List<VariableResp> boToResp(List<Variable> vars);

    /**
     * dto转entity
     * @param req 规则dto
     * @return entity
     */
    VariablePO toPo(VariableReq req);

    /**
     * dto转entity
     * @param req 规则dto
     * @return entity
     */
    List<VariablePO> toPo(List<VariableReq> req);
}
