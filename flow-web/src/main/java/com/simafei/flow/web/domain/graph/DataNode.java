package com.simafei.flow.web.domain.graph;

import com.simafei.flow.core.data.LoadSpec;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author fengpengju
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "数据节点")
public class DataNode extends GraphNode<LoadSpec> {

    @Schema(description = "数据加载配置")
    private LoadSpec loadSpec;

    @Override
    public LoadSpec extend() {
        return loadSpec;
    }
}
