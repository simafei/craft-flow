package com.simafei.flow.web.domain.req;

import com.simafei.flow.core.common.ExecStatus;
import com.simafei.flow.core.common.FlowStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author fengpengju
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FlowResultReq extends PageParam {

    @Schema(description = "执行决策流Id列表")
    private List<Long> flowIds;

    @Schema(description = "执行请求Id")
    private Long execId;

    @Schema(description = "执行状态")
    private ExecStatus execStatus;

    @Schema(description = "执行模式,0:正常,1:人工")
    private Integer execMode;

    @Schema(description = "决策结果,0:待定,1:通过,-1:拒绝")
    private FlowStatus flowStatus;

    @Schema(description = "测试模式,0:正常,1:测试")
    private Integer testMode;
}
