package com.simafei.flow.web.domain.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
*
* 算法模型表
* @author Generator
* @since 2023-12-26
*/
@Data
@Schema(description="执行API请求参数")
public class ApiExecReq implements Serializable {

    @Schema(description = "API Id")
    private Long apiId;

    @Schema(description = "API参数")
    private Map<String, Object> param;
}

