package com.simafei.flow.web.transfer;

import com.simafei.flow.web.domain.resp.GroupResp;
import com.simafei.flow.web.entity.GroupPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface GroupTransfer {

    GroupTransfer INSTANCE = Mappers.getMapper(GroupTransfer.class);

    /**
     * entity转dto
     * @param group 分组
     * @return 规则dto
     */
    @Mappings({
            @Mapping(source = "groupName", target = "name"),
            @Mapping(source = "groupDesc", target = "desc")
    })
    GroupResp toResp(GroupPO group);

    /**
     * entity转dto
     * @param req 模型
     * @return 规则dto
     */
    List<GroupResp> toResp(List<GroupPO> req);
}
