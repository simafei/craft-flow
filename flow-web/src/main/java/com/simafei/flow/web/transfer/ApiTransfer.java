package com.simafei.flow.web.transfer;

import com.simafei.flow.web.domain.req.ApiReq;
import com.simafei.flow.web.domain.resp.ApiResp;
import com.simafei.flow.web.entity.ApiPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface ApiTransfer {

    ApiTransfer INSTANCE = Mappers.getMapper(ApiTransfer.class);

    /**
     * entity转dto
     * @param api 模型
     * @return 规则dto
     */
    @Mapping(target = "serializeType", expression =
            "java( com.simafei.flow.core.api.SerializeType.of(api.getSerializeType()) )")
    ApiResp toResp(ApiPO api);

    /**
     * entity转dto
     * @param api 模型
     * @return 规则dto
     */
    List<ApiResp> toResp(List<ApiPO> api);

    /**
     * dto转entity
     * @param api 规则dto
     * @return entity
     */
    @Mapping(target = "serializeType", expression =
            "java( api.getSerializeType().getType() )")
    ApiPO toPo(ApiReq api);
}
