package com.simafei.flow.web.domain.graph;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author fengpengju
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "API节点")
public class ApiNode extends GraphNode<Long> {

    private Long apiId;

    @Override
    public Long extend() {
        return apiId;
    }
}
