package com.simafei.flow.web.transfer;

import com.simafei.flow.web.domain.resp.FlowResultResp;
import com.simafei.flow.web.entity.FlowResultPO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface FlowResultTransfer {

    FlowResultTransfer INSTANCE = Mappers.getMapper(FlowResultTransfer.class);

    /**
     * entity转dto
     * @param flowResult 流程执行结果
     * @return 流程执行结果
     */
    FlowResultResp toResp(FlowResultPO flowResult);

    /**
     * entity转dto
     * @param flowResults 流程执行结果
     * @return 流程执行结果
     */
    List<FlowResultResp> toResp(List<FlowResultPO> flowResults);
}
