package com.simafei.flow.web.domain.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author fengpengju
 */
@Data
public class FlowResp {

    @Schema(description = "主键ID")
    private Long id;

    @Schema(description = "前端图回显JSON")
    private String graph;

    @Schema(description = "决策流名称")
    private String flowName;

    @Schema(description = "场景分类Id")
    private Long categoryId;

    @Schema(description = "是否有效 0:无效 1:有效")
    private Integer valid;

    @Schema(description = "发布状态 0:未发布 1:审核中 2:已发布 3:已下线")
    private Integer status;

    @Schema(description = "版本Id")
    private Long groupId;

    @Schema(description = "权重")
    private Integer weight;

    @Schema(description = "版本号")
    private String versionNo;
}
