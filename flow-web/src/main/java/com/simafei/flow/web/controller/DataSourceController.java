package com.simafei.flow.web.controller;

import com.simafei.flow.web.common.CommonResult;
import com.simafei.flow.web.domain.req.DataSourceReq;
import com.simafei.flow.web.domain.req.LoadExecReq;
import com.simafei.flow.web.domain.req.PageParam;
import com.simafei.flow.web.domain.resp.DataSourceResp;
import com.simafei.flow.web.domain.resp.PageResult;
import com.simafei.flow.web.domain.resp.VariableResp;
import com.simafei.flow.web.service.IDataSourceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author fengpengju
 */
@RestController
@RequestMapping("/ds")
@Tag(name = "数据源管理")
@RequiredArgsConstructor
public class DataSourceController {

    private final IDataSourceService dataSourceService;

    @Operation(summary = "获取可用的数据源")
    @PostMapping("/page")
    public CommonResult<PageResult<DataSourceResp>> page(@RequestBody PageParam param) {
        return CommonResult.success(dataSourceService.listPage(param));
    }

    @Operation(summary = "获取数据源所有数据表")
    @GetMapping("/tables")
    public CommonResult<List<String>> tables(@RequestParam String dataSource,
                                             @RequestParam(required = false) String tableName) {
        return CommonResult.success(dataSourceService.tables(dataSource, tableName));
    }

    @Operation(summary = "获取数据表定义的字段列表")
    @GetMapping("/columns")
    public CommonResult<List<VariableResp>> columns(@RequestParam String dataSource, @RequestParam String tableName) {
        return CommonResult.success(dataSourceService.columns(dataSource, tableName));
    }

    @Operation(summary = "添加数据源")
    @PostMapping("/add")
    public CommonResult<Long> add(@Validated @RequestBody DataSourceReq dataSourceReq) {
        Long add = dataSourceService.add(dataSourceReq);
        return Objects.nonNull(add) ? CommonResult.success(add) :CommonResult.error("添加失败");
    }

    @Operation(summary = "删除数据源")
    @RequestMapping(value = "/delete", method = {RequestMethod.DELETE, RequestMethod.POST})
    public CommonResult<Boolean> delete(@RequestParam Long id) {
        boolean ret = dataSourceService.delete(id);
        return CommonResult.success(ret);
    }

    @Operation(summary = "执行数据加载配置获取数据")
    @PostMapping("/execute")
    public CommonResult<List<Map<String, Object>>> execute(@RequestBody LoadExecReq req) {
        List<Map<String, Object>> ret = dataSourceService.execute(req);
        return CommonResult.success(ret);
    }
}
