package com.simafei.flow.web.transfer;

import com.simafei.flow.core.EdgeResult;
import com.simafei.flow.core.NodeResult;
import com.simafei.flow.web.domain.resp.EdgeResultResp;
import com.simafei.flow.web.entity.EdgeResultPO;
import com.simafei.flow.web.entity.NodeResultPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface EdgeResultTransfer {

    EdgeResultTransfer INSTANCE = Mappers.getMapper(EdgeResultTransfer.class);

    /**
     * entity转dto
     * @param edgeResult 边执行结果
     * @return 边执行结果
     */
    EdgeResultResp toResp(EdgeResultPO edgeResult);

    /**
     * entity转dto
     * @param edgeResults 边执行结果
     * @return 边执行结果
     */
    List<EdgeResultResp> toResp(List<EdgeResultPO> edgeResults);

    /**
     * entity转bo
     * @param result 节点执行结果
     * @return 节点
     */
    @Mapping(target = "inputParams", source = "execParams")
    EdgeResult toBo(EdgeResultPO result);


    /**
     * entity转bo
     * @param results 边执行结果
     * @return 边执行结果
     */
    List<EdgeResult> toBo(List<EdgeResultPO> results);
}
