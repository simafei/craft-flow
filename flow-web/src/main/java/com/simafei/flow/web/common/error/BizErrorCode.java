package com.simafei.flow.web.common.error;

/**
 * @author fengpengju
 */
public interface BizErrorCode {

    ErrorCode FLOW_NOT_EXIST = new ErrorCode(1001, "流程{}不存在");
    ErrorCode DUPLICATE_KEY_ERROR = new ErrorCode(1002, "名称[{}]已定义");
    ErrorCode DATA_SOURCE_CONNECT_FAILURE = new ErrorCode(1003, "数据源配置连接失败, error={}");
    ErrorCode DRIVER_CLASS_NOT_FOUND = new ErrorCode(1004, "数据源驱动[{}]不存在");
    ErrorCode DATA_SOURCE_NOT_DEFINED = new ErrorCode(1005, "数据源[{}]未配置");
    ErrorCode DATABASE_EMPTY = new ErrorCode(1006, "URL中的数据库配置为空");
    ErrorCode FLOW_RESUME_ERROR = new ErrorCode(1007, "流程恢复执行失败，未找到对应的执行信息");
    ErrorCode DUPLICATE_VAR_NAMES = new ErrorCode(1008, "变量名重复，varNames={}");
}
