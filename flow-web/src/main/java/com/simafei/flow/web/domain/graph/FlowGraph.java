package com.simafei.flow.web.domain.graph;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

/**
 * @author fengpengju
 */
@Data
public class FlowGraph {

    @Schema(description = "流程id")
    private Long flowId;

    @Schema(description = "流程图")
    @NotNull(message = "流程图不能为空")
    private String graph;

    @Schema(description = "节点列表")
    @NotEmpty(message = "节点列表不能为空")
    private List<GraphNode<?>> nodes;

    @Schema(description = "边列表")
    @NotEmpty(message = "边列表不能为空")
    private List<GraphEdge> edges;
}
