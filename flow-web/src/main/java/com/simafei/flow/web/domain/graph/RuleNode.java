package com.simafei.flow.web.domain.graph;

import com.simafei.flow.core.rule.FlowRules;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author fengpengju
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "规则节点")
public class RuleNode extends GraphNode<FlowRules> {

    @Schema(description = "流程规则")
    private FlowRules flowRules;

    @Override
    public FlowRules extend() {
        return flowRules;
    }
}
