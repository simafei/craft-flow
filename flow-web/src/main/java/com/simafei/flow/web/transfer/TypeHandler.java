package com.simafei.flow.web.transfer;

import cn.hutool.core.map.MapUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.simafei.flow.core.ExecutionResult;
import com.simafei.flow.core.common.Assignment;
import com.simafei.flow.core.common.CalcType;
import com.simafei.flow.core.common.Criteria;
import com.simafei.flow.core.common.FieldScope;
import com.simafei.flow.core.common.FieldType;
import com.simafei.flow.core.data.AggColumn;
import com.simafei.flow.core.json.JsonUtils;
import com.simafei.flow.web.domain.req.VariableReq;
import com.simafei.flow.web.domain.resp.VariableResp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author fengpengju
 */
public class TypeHandler {

    public Integer toValue(FieldType fieldType) {
        if (fieldType == null) {
            return null;
        }
        return fieldType.getValue();
    }

    public FieldType parseFieldType(Integer fieldType) {
        if (fieldType == null) {
            return null;
        }
        return FieldType.of(fieldType);
    }

    public Integer toValue(CalcType fieldType) {
        if (fieldType == null) {
            return null;
        }
        return fieldType.getValue();
    }

    public CalcType parseCalcType(Integer calcType) {
        if (calcType == null) {
            return null;
        }
        return CalcType.of(calcType);
    }

    public Integer toValue(FieldScope fieldScope) {
        if (fieldScope == null) {
            return null;
        }
        return fieldScope.getValue();
    }

    public FieldScope parseFieldScope(Integer fieldScope) {
        if (fieldScope == null) {
            return null;
        }
        return FieldScope.of(fieldScope);
    }

    public String toString(Criteria criteria) {
        if (criteria == null) {
            return null;
        }
        return JsonUtils.toJsonString(criteria);
    }

    public Criteria toCriteria(String str) {
        if (str == null) {
            return null;
        }
        return JsonUtils.parseObject(str, new TypeReference<>() {
        });
    }

    public String amToString(List<Assignment> assignments) {
        if (assignments == null) {
            return null;
        }
        return JsonUtils.toJsonString(assignments);
    }

    public List<Assignment> toAssignments(String str) {
        if (str == null) {
            return new ArrayList<>();
        }
        return JsonUtils.parseObject(str, new TypeReference<>() {
        });
    }

    public String resultsToString(List<ExecutionResult> results) {
        if (results == null) {
            return null;
        }
        return JsonUtils.toJsonString(results);
    }

    public List<ExecutionResult> toResults(String str) {
        if (str == null) {
            return new ArrayList<>();
        }
        return JsonUtils.parseArray(str, ExecutionResult.class);
    }

    public List<String> toList(String str) {
        return JsonUtils.parseArray(str, String.class);
    }

    public String toString(List<String> strings) {
        if (strings == null) {
            return null;
        }
        return JsonUtils.toJsonString(strings);
    }

    public String longsToString(List<Long> strings) {
        if (strings == null) {
            return null;
        }
        return JsonUtils.toJsonString(strings);
    }

    public List<Long> strToLongs(String str) {
        return JsonUtils.parseArray(str, Long.class);
    }

    public Set<String> toSet(String str) {
        if (str == null) {
            return new HashSet<>();
        }
        return new HashSet<>(JsonUtils.parseArray(str, String.class));
    }

    public String toString(Set<String> strings) {
        if (strings == null) {
            return null;
        }
        return JsonUtils.toJsonString(strings);
    }

    public List<AggColumn> toColumns(String str) {
        if (str == null) {
            return new ArrayList<>();
        }
        return JsonUtils.parseArray(str, AggColumn.class);
    }

    public String colToString(List<AggColumn> columns) {
        if (columns == null) {
            return null;
        }
        return JsonUtils.toJsonString(columns);
    }


    public List<VariableResp> toVars(String str) {
        if (str == null) {
            return new ArrayList<>();
        }
        return JsonUtils.parseArray(str, VariableResp.class);
    }

    public String varsToString(List<VariableReq> vars) {
        if (vars == null) {
            return null;
        }
        return JsonUtils.toJsonString(vars);
    }

    public List<Map<String, Object>> toParams(String str) {
        if (str == null) {
            return new ArrayList<>();
        }
        return JsonUtils.parseObject(str, new TypeReference<>() {
        });
    }

    public String paramsToStr(List<Map<String, Object>> params) {
        if (params == null) {
            return null;
        }
        return JsonUtils.toJsonString(params);
    }

    public Map<String, Object> toMap(String str) {
        if (str == null) {
            return MapUtil.newHashMap();
        }
        return JsonUtils.parseObject(str, new TypeReference<>() {
        });
    }

    public String mapToStr(Map<String, Object> params) {
        if (params == null) {
            return null;
        }
        return JsonUtils.toJsonString(params);
    }

}
