package com.simafei.flow.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.simafei.flow.core.common.ExecStatus;
import com.simafei.flow.core.common.NodeType;
import com.simafei.flow.web.dao.NodeResultMapper;
import com.simafei.flow.web.entity.NodeResultPO;
import com.simafei.flow.web.service.INodeResultService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
@Service
public class NodeResultServiceImpl extends ServiceImpl<NodeResultMapper, NodeResultPO> implements INodeResultService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void replace(NodeResultPO po) {
        // 如果节点状态从等待中恢复，则删除原来等待的结果(可以理解为更新)
        // 后续改完INSERT ON DUPLICATE KEY UPDATE
        if (NodeType.MANUAL == NodeType.valueOf(po.getNodeType())) {
            remove(new LambdaQueryWrapper<NodeResultPO>()
                    .eq(NodeResultPO::getExecId, po.getExecId())
                    .eq(NodeResultPO::getNodeId, po.getNodeId())
                    .eq(NodeResultPO::getParentId, po.getParentId())
                    .eq(NodeResultPO::getExecStatus, ExecStatus.WAITING.name()));
        }
        save(po);
    }
}
