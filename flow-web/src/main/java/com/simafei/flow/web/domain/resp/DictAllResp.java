package com.simafei.flow.web.domain.resp;

import lombok.Data;

import java.util.List;

/**
 * @author fengpengju
 */
@Data
public class DictAllResp {

    private List<DictResp> operators;
    private List<DictResp> listOperators;
    private List<DictResp> fieldTypes;
    private List<DictResp> calcTypes;
    private List<DictResp> conjs;
    private List<DictResp> fieldScopes;
    private List<DictResp> aggFuncs;
    private List<DictResp> nodeTypes;
    private List<DictResp> serializeTypes;
    private List<DictResp> apiTemplates;
    private List<DictResp> execStatuses;
}
