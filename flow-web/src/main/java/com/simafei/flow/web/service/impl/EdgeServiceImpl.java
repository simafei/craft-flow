package com.simafei.flow.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.simafei.flow.web.dao.EdgeMapper;
import com.simafei.flow.web.entity.EdgePO;
import com.simafei.flow.web.service.IEdgeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 决策流节点连线 服务实现类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
@Service
public class EdgeServiceImpl extends ServiceImpl<EdgeMapper, EdgePO> implements IEdgeService {

}
