package com.simafei.flow.web.domain.req;

import com.simafei.flow.core.api.SerializeType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.util.List;

/**
*
* 算法模型表
* @author Generator
* @since 2023-12-26
*/
@Data
@Schema(description="API新增")
public class ApiReq implements Serializable {

    @Schema(description = "模型名称")
    @NotNull(message = "模型名称不能为空")
    @Length(max = 64, message = "模型名称长度不能超过255")
    private String apiName;

    @Schema(description = "模型描述")
    @Length(max = 255, message = "模型描述长度不能超过255")
    private String apiDesc;

    @Schema(description = "变量类型，0: 自定义入参，1：依赖前置节点")
    private Integer varMode;

    @Schema(description = "使用的解析模板")
    private String template;

    @Schema(description = "模型调用地址")
    @Pattern(regexp = "^http(s)?://.*", message = "httpUrl格式不正确")
    @NotNull(message = "模型调用地址不能为空")
    @Length(max = 255, message = "模型调用地址长度不能超过255")
    private String httpUrl;

    @Schema(description = "HTTP调用方法")
    @Pattern(regexp = "^(GET|POST)$", message = "Invalid HTTP method")
    @NotNull(message = "HTTP调用方法不能为空")
    private String httpMethod;

    @Schema(description = "组装请求参数和Header的脚本")
    private String serializeScript;

    @Schema(description = "序列化类型")
    @NotNull(message = "序列化类型不能为空")
    private SerializeType serializeType;

    @Schema(description = "模型入参变量列表")
    private List<VariableReq> reqVariables;
}

