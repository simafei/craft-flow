package com.simafei.flow.web.domain.req;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 决策流
 *
 * @author Generator
 * @since 2023-12-25
 */
@Data
@Schema(description = "决策流-传输对象")
public class FlowReq implements Serializable {

    @Schema(description = "入参变量列表")
    private List<VariableReq> variables;

    @Schema(description = "决策流名称")
    @NotNull(message = "决策流名称不能为空")
    private String flowName;

    @Schema(description = "决策流说明")
    private String flowDesc;

    @Schema(description = "场景分类Id")
    private Long categoryId;
}

