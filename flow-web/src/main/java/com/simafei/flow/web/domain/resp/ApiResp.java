package com.simafei.flow.web.domain.resp;

import com.simafei.flow.core.api.SerializeType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
*
* 算法模型表
* @author Generator
* @since 2024-01-22
*/
@Data
@Schema(description="API返回对象")
public class ApiResp implements Serializable {

    @Schema(description = "主键ID")
    private Long id;

    @Schema(description = "API名称")
    private String apiName;

    @Schema(description = "模型描述")
    private String apiDesc;

    @Schema(description = "模型状态, 0: 未发布, 1: 已发布(先默认传1)")
    private Integer apiStatus;

    @Schema(description = "变量类型，0: 自定义入参，1：依赖前置节点")
    private Integer varMode;

    @Schema(description = "使用的解析模板")
    private String template;

    @Schema(description = "API调用地址")
    private String httpUrl;

    @Schema(description = "HTTP调用方法")
    private String httpMethod;

    @Schema(description = "上一次执行API入参")
    private Map<String, Object> paramsJson;

    @Schema(description = "场景分类id，为0时，代表是通用模型")
    private Long categoryId;

    @Schema(description = "组装请求参数和Header的脚本")
    private String serializeScript;

    @Schema(description = "序列化类型，JSON, XML")
    private SerializeType serializeType;

    @Schema(description = "API入参变量列表")
    private List<VariableResp> reqVariables;

    @Schema(description = "API结果变量列表")
    private List<VariableResp> respVariables;
}

