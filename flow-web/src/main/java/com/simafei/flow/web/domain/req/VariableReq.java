package com.simafei.flow.web.domain.req;

import com.simafei.flow.core.common.FieldScope;
import com.simafei.flow.core.common.FieldType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author fengpengju
 */
@Data
public class VariableReq {

    @Schema(description = "字段名称")
    @NotNull(message = "字段名称不能为空")
    private String varName;

    @Schema(description = "字段中文名")
    @NotNull(message = "字段中文名不能为空")
    private String varLabel;

    @Schema(description = "字段类型 0: 字符, 1: 整数, 2:浮点数")
    @NotNull(message = "字段类型不能为空")
    private FieldType varType;

    @Schema(description = "字段作用域 0: 请求, 1: 响应")
    private FieldScope fieldScope;

    @Schema(description = "初始值")
    private String initValue;
}
