package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.web.entity.EdgeResultPO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface IEdgeResultService extends IService<EdgeResultPO> {

}
