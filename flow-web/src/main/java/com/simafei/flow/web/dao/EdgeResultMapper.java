package com.simafei.flow.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simafei.flow.web.entity.EdgeResultPO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface EdgeResultMapper extends BaseMapper<EdgeResultPO> {

}
