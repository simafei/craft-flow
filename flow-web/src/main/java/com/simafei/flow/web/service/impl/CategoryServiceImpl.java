package com.simafei.flow.web.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.simafei.flow.web.dao.CategoryMapper;
import com.simafei.flow.web.domain.req.CategoryReq;
import com.simafei.flow.web.entity.CategoryPO;
import com.simafei.flow.web.service.ICategoryService;
import com.simafei.flow.web.transfer.CategoryTransfer;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 场景分类 服务实现类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, CategoryPO> implements ICategoryService {

    @Override
    public Long add(CategoryReq req) {
        CategoryPO po = CategoryTransfer.INSTANCE.toPo(req);
        save(po);
        return po.getId();
    }

    @Override
    public boolean delete(Long id) {
        return removeById(id);
    }

    @Override
    public boolean rename(Long id, String name, String icon) {
        return update(new LambdaUpdateWrapper<>(CategoryPO.class)
                .eq(CategoryPO::getId, id)
                .set(CategoryPO::getName, name)
                .set(StrUtil.isNotBlank(icon), CategoryPO::getIcon, icon));
    }
}
