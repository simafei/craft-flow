package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.web.domain.req.DataSourceReq;
import com.simafei.flow.web.domain.req.LoadExecReq;
import com.simafei.flow.web.domain.req.PageParam;
import com.simafei.flow.web.domain.resp.DataSourceResp;
import com.simafei.flow.web.domain.resp.PageResult;
import com.simafei.flow.web.domain.resp.VariableResp;
import com.simafei.flow.web.entity.DataSourcePO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-15
 */
public interface IDataSourceService extends IService<DataSourcePO> {

    /**
     * 获取可用的数据源
     * @param param 分页参数
     * @return 分页数据
     */
    PageResult<DataSourceResp> listPage(PageParam param);

    /**
     * 获取数据源所有数据表
     * @param dataSource 数据源
     * @param tableName 表名
     * @return 表名列表
     */
    List<String> tables(String dataSource, String tableName);

    /**
     * 获取数据表定义的字段列表
     * @param dataSource 数据源
     * @param tableName 表名
     * @return 字段列表
     */
    List<VariableResp> columns(String dataSource, String tableName);

    /**
     * 添加数据源
     * @param dataSourceReq 数据源
     * @return 数据源id
     */
    Long add(DataSourceReq dataSourceReq);

    /**
     * 删除数据源
     * @param id 数据源id
     * @return 是否删除
     */
    boolean delete(Long id);

    /**
     * 执行数据加载配置获取数据
     * @param req 数据加载配置
     * @return 数据
     */
    List<Map<String, Object>> execute(LoadExecReq req);
}
