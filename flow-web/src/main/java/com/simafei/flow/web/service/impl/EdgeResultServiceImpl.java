package com.simafei.flow.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.simafei.flow.web.dao.EdgeResultMapper;
import com.simafei.flow.web.entity.EdgeResultPO;
import com.simafei.flow.web.service.IEdgeResultService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
@Service
public class EdgeResultServiceImpl extends ServiceImpl<EdgeResultMapper, EdgeResultPO> implements IEdgeResultService {

}
