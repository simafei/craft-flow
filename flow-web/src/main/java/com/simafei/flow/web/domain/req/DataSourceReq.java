package com.simafei.flow.web.domain.req;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-15
 */
@Data
public class DataSourceReq implements Serializable {

    @Schema(description = "数据源名称")
    @NotBlank(message = "数据源名称不能为空")
    private String dsName;

    @Schema(description = "连接url")
    @NotBlank(message = "连接url不能为空")
    private String url;

    @Schema(description = "驱动类")
    @NotBlank(message = "驱动类不能为空")
    private String driverClass;

    @Schema(description = "用户名")
    @NotBlank(message = "用户名不能为空")
    private String username;

    @Schema(description = "密码")
    @NotBlank(message = "密码不能为空")
    private String password;
}
