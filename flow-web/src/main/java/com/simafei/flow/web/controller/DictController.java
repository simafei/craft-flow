package com.simafei.flow.web.controller;

import com.simafei.flow.core.api.SerializeType;
import com.simafei.flow.core.api.Templates;
import com.simafei.flow.core.common.CalcType;
import com.simafei.flow.core.common.Conj;
import com.simafei.flow.core.common.ExecStatus;
import com.simafei.flow.core.common.FieldScope;
import com.simafei.flow.core.common.FieldType;
import com.simafei.flow.core.common.ListOperator;
import com.simafei.flow.core.common.NodeType;
import com.simafei.flow.core.common.Operator;
import com.simafei.flow.core.data.AggFunc;
import com.simafei.flow.core.func.Funcs;
import com.simafei.flow.web.common.CommonResult;
import com.simafei.flow.web.domain.resp.DictAllResp;
import com.simafei.flow.web.domain.resp.DictResp;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fengpengju
 */
@RestController
@RequestMapping("/dict")
@Tag(name = "字典接口")
public class DictController {

    @Operation(summary = "操作符字典")
    @GetMapping("/operator")
    public CommonResult<List<DictResp>> getOperatorDict() {
        List<DictResp> dicts = operatorDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "获取集合操作符字典")
    @GetMapping("/listOperator")
    public CommonResult<List<DictResp>> getListOperatorDict() {
        List<DictResp> dicts = listOperatorDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "字段类型字典")
    @GetMapping("/field-type")
    public CommonResult<List<DictResp>> getFieldTypeDict() {
        List<DictResp> dicts = fieldTypeDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "计算类型字典")
    @GetMapping("/calc-type")
    public CommonResult<List<DictResp>> getCalcTypeDict() {
        List<DictResp> dicts = calcTypeDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "连接类型字典")
    @GetMapping("/conj")
    public CommonResult<List<DictResp>> getConjDict() {
        List<DictResp> dicts = conjDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "字段作用域字典")
    @GetMapping("/field-scope")
    public CommonResult<List<DictResp>> getFieldScopeDict() {
        List<DictResp> dicts = fieldScopeDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "获取聚合函数字典")
    @GetMapping("/agg-func")
    public CommonResult<List<DictResp>> getAggFuncDict() {
        List<DictResp> dicts = aggFuncDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "获取节点类型字典")
    @GetMapping("/node-type")
    public CommonResult<List<DictResp>> getNodeTypeDict() {
        List<DictResp> dicts = nodeTypeDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "获取序列号类型字典")
    @GetMapping("/serialize-type")
    public CommonResult<List<DictResp>> getSerializeTypeDict() {
        List<DictResp> dicts = serializeTypeDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "获取API解析模板字典")
    @GetMapping("/api-template")
    public CommonResult<List<DictResp>> getApiTemplateDict() {
        List<DictResp> dicts = apiTemplateDicts();
        return CommonResult.success(dicts);
    }

    @Operation(summary = "获取执行状态字典")
    @GetMapping("/exec-status")
    public CommonResult<List<DictResp>> getExecStatusDict() {
        List<DictResp> dicts = execStatusDicts();
        return CommonResult.success(dicts);
    }

    private List<DictResp> operatorDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (Operator value : Operator.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.getDesc());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> listOperatorDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (ListOperator value : ListOperator.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.getDesc());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> fieldTypeDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (FieldType value : FieldType.values()) {
            if (value.isEnabled()) {
                DictResp dict = new DictResp();
                dict.setDictCode(value.name());
                dict.setDictName(value.getDesc());
                dict.setEnable(true);
                dicts.add(dict);
            }
        }
        return dicts;
    }

    private List<DictResp> calcTypeDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (CalcType value : CalcType.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.getDesc());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> conjDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (Conj value : Conj.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.getDesc());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> fieldScopeDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (FieldScope value : FieldScope.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.getDesc());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> aggFuncDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (AggFunc value : AggFunc.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.getDesc());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> nodeTypeDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (NodeType value : NodeType.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.name());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> serializeTypeDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (SerializeType value : SerializeType.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.name());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> apiTemplateDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (Templates value : Templates.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.getName());
            dict.setDictName(value.getDesc());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    private List<DictResp> execStatusDicts() {
        List<DictResp> dicts = new ArrayList<>();
        for (ExecStatus value : ExecStatus.values()) {
            DictResp dict = new DictResp();
            dict.setDictCode(value.name());
            dict.setDictName(value.getDesc());
            dict.setEnable(true);
            dicts.add(dict);
        }
        return dicts;
    }

    @Operation(summary = "获取所有字典数据")
    @GetMapping("/all")
    public CommonResult<DictAllResp> getAllDicts() {
        DictAllResp response = new DictAllResp();

        response.setOperators(operatorDicts());
        response.setListOperators(listOperatorDicts());
        response.setFieldTypes(fieldTypeDicts());
        response.setCalcTypes(calcTypeDicts());
        response.setConjs(conjDicts());
        response.setFieldScopes(fieldScopeDicts());
        response.setAggFuncs(aggFuncDicts());
        response.setNodeTypes(nodeTypeDicts());
        response.setSerializeTypes(serializeTypeDicts());
        response.setApiTemplates(apiTemplateDicts());
        response.setExecStatuses(execStatusDicts());

        return CommonResult.success(response);
    }

    @GetMapping("/funcs")
    @Operation(summary = "获取可执行函数(表达式)列表")
    public CommonResult<List<String>> getFuncs() {
        return CommonResult.success(Funcs.funcList());
    }
}