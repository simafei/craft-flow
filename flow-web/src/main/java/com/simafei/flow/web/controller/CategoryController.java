package com.simafei.flow.web.controller;

import com.simafei.flow.web.common.CommonResult;
import com.simafei.flow.web.domain.req.CategoryReq;
import com.simafei.flow.web.service.ICategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 场景分类 前端控制器
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
@RestController
@RequestMapping("/category")
@Tag(name = "场景分类")
@RequiredArgsConstructor
public class CategoryController {

    private final ICategoryService categoryService;

    @Operation(summary = "新增场景分类")
    @PostMapping("/add")
    public CommonResult<Long> add(@Validated @RequestBody CategoryReq req) {
        return CommonResult.success(categoryService.add(req));
    }

    @Operation(summary = "删除场景分类")
    @PostMapping("/delete")
    public CommonResult<Void> delete(@RequestParam Long id) {
        boolean ret = categoryService.delete(id);
        return ret ? CommonResult.success() : CommonResult.error();
    }

    @Operation(summary = "场景分类重命名")
    @PostMapping("/rename")
    public CommonResult<Boolean> rename(@RequestParam Long categoryId,
                                        @RequestParam String name,
                                        @RequestParam(required = false) String icon) {
        return CommonResult.success(categoryService.rename(categoryId, name, icon));
    }
}
