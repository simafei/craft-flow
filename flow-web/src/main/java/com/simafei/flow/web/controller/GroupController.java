package com.simafei.flow.web.controller;


import com.simafei.flow.web.common.CommonResult;
import com.simafei.flow.web.domain.resp.CategoryResp;
import com.simafei.flow.web.domain.resp.FlowResultResp;
import com.simafei.flow.web.service.IGroupService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * http api 入口
 *
 * @author generator
 * @since 2023-05-05
 */
@Tag(name = "流程分组")
@RestController
@RequestMapping("/group")
@RequiredArgsConstructor
public class GroupController {

    private final IGroupService groupService;

    @Operation(summary = "根据分类查询决策流列表，并分组展示")
    @PostMapping("/listWithCategory")
    public CommonResult<List<CategoryResp>> listWithCategory() {
        return CommonResult.success(groupService.listWithCategory());
    }

    @Operation(summary = "执行流程")
    @PostMapping("/{groupId}/execute")
    public CommonResult<FlowResultResp> execute(@Parameter(description = "分组Id") @PathVariable Long groupId,
                                                @RequestBody Map<String, Object> param) {
        return CommonResult.success(groupService.execute(groupId, param));
    }

    @Operation(summary = "异步执行决策流")
    @PostMapping("/{groupId}/executeAsync")
    public CommonResult<String> executeAsync(@Parameter(description = "groupId") @PathVariable Long groupId,
                                                  @RequestBody Map<String, Object> param) {

        return CommonResult.success(groupService.executeAsync(groupId, param));
    }

    @Operation(summary = "重命名决策流")
    @PostMapping("/{groupId}/rename")
    public CommonResult<Boolean> rename(@Parameter(description = "分组Id") @PathVariable Long groupId,
                                        @RequestParam String groupName,
                                        @RequestParam String groupDesc) {

        return CommonResult.success(groupService.rename(groupId, groupName, groupDesc));
    }

}
