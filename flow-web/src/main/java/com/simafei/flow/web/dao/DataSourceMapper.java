package com.simafei.flow.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simafei.flow.web.entity.DataSourcePO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-15
 */
public interface DataSourceMapper extends BaseMapper<DataSourcePO> {

}
