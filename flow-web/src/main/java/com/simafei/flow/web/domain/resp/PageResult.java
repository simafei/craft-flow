package com.simafei.flow.web.domain.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Schema(description = "分页结果")
@Data
public final class PageResult<T> implements Serializable {

    @Schema(description = "数据")
    private List<T> list;

    @Schema(description = "总量")
    private Long total;

    @Schema(description = "每页多少条")
    protected Long pageSize;

    @Schema(description = "当前页码")
    protected Long currentPage;

    @Schema(description = "总页数")
    protected Long totalPages;


    public PageResult() {

    }

    public PageResult(List<T> list, Long total) {
        this.list = list;
        this.total = total;
    }

    public PageResult(Long total) {
        this.list = new ArrayList<>();
        this.total = total;
    }

    public <U> PageResult<U> convert(Function<T, U> converter) {
        PageResult<U> res = new PageResult<>();
        res.total = this.total;
        res.pageSize = this.pageSize;
        res.currentPage = this.currentPage;
        res.totalPages = this.totalPages;
        res.list = this.list.stream().map(converter).collect(Collectors.toList());
        return res;
    }

    public static <T> PageResult<T> empty() {
        return new PageResult<>(0L);
    }

    public static <T> PageResult<T> empty(Long total) {
        return new PageResult<>(total);
    }

}
