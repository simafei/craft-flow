package com.simafei.flow.web.domain.graph;

import com.simafei.flow.core.data.AggSpec;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author fengpengju
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "聚合节点")
public class AggNode extends GraphNode<AggSpec> {

    @Schema(description = "聚合节点")
    private AggSpec aggSpec;

    @Override
    public AggSpec extend() {
        return aggSpec;
    }
}
