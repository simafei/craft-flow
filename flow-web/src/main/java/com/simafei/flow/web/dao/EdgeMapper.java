package com.simafei.flow.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.simafei.flow.web.entity.EdgePO;

/**
 * <p>
 * 决策流节点连线 Mapper 接口
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface EdgeMapper extends BaseMapper<EdgePO> {

}
