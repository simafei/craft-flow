package com.simafei.flow.web.service;

import com.github.yulichang.base.MPJBaseService;
import com.simafei.flow.web.domain.req.FlowResultReq;
import com.simafei.flow.web.domain.resp.FlowResultResp;
import com.simafei.flow.web.domain.resp.FrDetailResp;
import com.simafei.flow.web.domain.resp.PageResult;
import com.simafei.flow.web.entity.FlowResultPO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface IFlowResultService extends MPJBaseService<FlowResultPO> {

    /**
     * 查询决策流执行结果
     * @param execId 执行请求id
     * @return FlowResultResp
     */
    FlowResultResp getByExecId(String execId);

    /**
     * 分页查询决策流执行结果
     * @param req 分页请求
     * @return 分页请求
     */
    PageResult<FlowResultResp> queryPage(FlowResultReq req);

    /**
     * 查询决策流执行结果详情
     * @param execId 执行请求id
     * @return FrDetailResp
     */
    FrDetailResp detail(String execId);

    /**
     * 插入或更新
     * @param result FlowResultPO
     */
    void replace(FlowResultPO result);
}
