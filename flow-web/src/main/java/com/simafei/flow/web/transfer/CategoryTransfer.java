package com.simafei.flow.web.transfer;

import com.simafei.flow.web.domain.req.CategoryReq;
import com.simafei.flow.web.entity.CategoryPO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface CategoryTransfer {

    CategoryTransfer INSTANCE = Mappers.getMapper(CategoryTransfer.class);

    /**
     * dto转entity
     * @param req 规则dto
     * @return entity
     */
    CategoryPO toPo(CategoryReq req);
}
