package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 决策流节点
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("node")
public class NodePO extends BasePO {


    /**
     * 节点Id(前端生成)
     */
    @TableField("node_id")
    private String nodeId;

    /**
     * 决策流Id
     */
    @TableField("flow_id")
    private Long flowId;

    /**
     * 节点名称
     */
    @TableField("node_name")
    private String nodeName;

    /**
     * 个性化数据
     */
    @TableField("extend")
    private String extend;

    /**
     * 节点类型, START,END,RULE,MANUAL,DATA_LOAD,DATA_MODIFY
     */
    @TableField("node_type")
    private String nodeType;

    /**
     * 是否阻塞
     */
    @TableField("blocked")
    private Boolean blocked;

    /**
     * 透传字段
     */
    @TableField("payload")
    private String payload;

    /**
     * 节点私有参数
     */
    @TableField("variables")
    private String variables;
}
