package com.simafei.flow.web.domain.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
*
* 场景分类
* @author Generator
* @since 2023-12-19
*/
@Data
@Schema(description="分类-流程组-流程")
public class CategoryResp implements Serializable {

    @Schema(description = "主键ID")
    private Long id;


    @Schema(description = "分类名称")
    private String name;


    @Schema(description = "icon")
    private String icon;


    @Schema(description = "分组列表")
    private List<GroupResp> groups;

}

