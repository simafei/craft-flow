package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-20
 */
@Getter
@Setter
@TableName("flow_group")
public class GroupPO extends BasePO {


    /**
     * 场景分类Id
     */
    @TableField("category_id")
    private Long categoryId;

    /**
     * 流程名称
     */
    @TableField("group_name")
    private String groupName;

    /**
     * 流程说明
     */
    @TableField("group_desc")
    private String groupDesc;
}
