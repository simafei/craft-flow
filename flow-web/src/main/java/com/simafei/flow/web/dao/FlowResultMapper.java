package com.simafei.flow.web.dao;

import com.github.yulichang.base.MPJBaseMapper;
import com.simafei.flow.web.entity.FlowResultPO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface FlowResultMapper extends MPJBaseMapper<FlowResultPO> {

}
