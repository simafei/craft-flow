package com.simafei.flow.web.domain.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author Generator
 * @since 2024-02-28
 */
@Data
@Schema(description = "流程分组-流程")
public class GroupResp implements Serializable {

    @Schema(description = "主键ID")
    private Long id;

    @Schema(description = "场景分类Id")
    private Long categoryId;

    @Schema(description = "流程名称")
    private String name;

    @Schema(description = "分组说明")
    private String desc;

    @Schema(description = "所有版本")
    private List<FlowResp> flows;

    @Schema(description = "是否为空", hidden = true)
    private Boolean empty;
}

