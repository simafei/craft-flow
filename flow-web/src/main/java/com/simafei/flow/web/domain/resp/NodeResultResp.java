package com.simafei.flow.web.domain.resp;

import com.simafei.flow.core.common.ExecStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-20
 */
@Getter
@Setter
public class NodeResultResp {

    /**
     * 执行请求id
     */
    @Schema(description = "执行请求id")
    private String execId;

    /**
     * 执行规则id
     */
    @Schema(description = "执行规则id")
    private String flowId;

    /**
     * 执行节点Id
     */
    @Schema(description = "执行节点Id")
    private String nodeId;

    /**
     * 上一执行节点
     */
    @Schema(description = "上一执行节点")
    private String parentId;

    /**
     * 节点类型
     */
    @Schema(description = "节点类型")
    private String nodeType;

    /**
     * 异常信息
     */
    @Schema(description = "异常信息")
    private String errorMsg;

    /**
     * 执行参数
     */
    @Schema(description = "执行参数")
    private List<Map<String, Object>> execParams;

    /**
     * 执行结果
     */
    @Schema(description = "执行结果")
    private List<Map<String, Object>> execResult;

    /**
     * 输出变量列表
     */
    @Schema(description = "输出变量列表")
    private String outputVars;

    /**
     * 是否成功
     */
    @Schema(description = "是否成功")
    private Boolean success;

    /**
     * 开始时间
     */
    @Schema(description = "开始时间")
    private Long startTime;

    /**
     * 结束时间
     */
    @Schema(description = "结束时间")
    private Long endTime;

    /**
     * 执行状态
     */
    @Schema(description = "执行状态")
    private ExecStatus execStatus;
}
