package com.simafei.flow.web.domain.req;

import com.simafei.flow.core.common.FlowStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.Map;

/**
 * @author fengpengju
 */
@Data
public class ResumeReq {

    @Schema(description = "节点Id")
    @NotBlank(message = "节点Id不能为空")
    private String nodeId;

    @Schema(description = "执行Id")
    @NotBlank(message = "执行Id不能为空")
    private String execId;

    @Schema(description = "流程Id")
    @NotBlank(message = "流程Id不能为空")
    private Long flowId;

    @Schema(description = "父Id")
    @NotBlank(message = "父Id不能为空")
    private String parentId;

    @Schema(description = "执行状态, REJECT:拒绝, PASS:通过")
    @NotBlank(message = "执行状态不能为空")
    private FlowStatus flowStatus;

    @Schema(description = "执行参数")
    private Map<String, Object> execParams;
}
