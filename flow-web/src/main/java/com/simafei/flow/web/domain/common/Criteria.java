package com.simafei.flow.web.domain.common;

import com.simafei.flow.core.common.Conj;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author fengpengju
 */
@Schema(description = "执行条件")
@Data
public class Criteria implements Condition {
    @Schema(description = "条件类型(分组还是单个条件)", allowableValues = {"Criteria", "Criterion"})
    private String type;

    @Schema(description = "条件连接符")
    private Conj conj;

    @Schema(description = "条件列表", subTypes = {Criteria.class, Criterion.class})
    private List<Condition> conditions;
}
