package com.simafei.flow.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.simafei.flow.core.Flow;
import com.simafei.flow.web.domain.graph.FlowGraph;
import com.simafei.flow.web.domain.req.FlowExecReq;
import com.simafei.flow.web.domain.req.FlowReq;
import com.simafei.flow.web.domain.req.PageParam;
import com.simafei.flow.web.domain.req.ResumeReq;
import com.simafei.flow.web.domain.resp.FlowResp;
import com.simafei.flow.web.domain.resp.FlowResultResp;
import com.simafei.flow.web.domain.resp.PageResult;
import com.simafei.flow.web.domain.resp.VariableResp;
import com.simafei.flow.web.entity.FlowPO;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 决策流 服务类
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-18
 */
public interface IFlowService extends IService<FlowPO> {

    /**
     * 新增决策流
     *
     * @param req 请求参数
     * @return 响应参数
     */
    FlowResp add(FlowReq req);

    /**
     * 新增决策流
     *
     * @param graph 请求参数
     * @return 响应参数
     */
    FlowResp addGraph(FlowGraph graph);

    /**
     * 获取决策流
     *
     * @param flowId 决策流ID
     * @return 响应参数
     */
    FlowResp get(Long flowId);

    /**
     * 获取决策流
     *
     * @param flowId 决策流ID
     * @return 响应参数
     */
    FlowResp getSimple(Long flowId);

    /**
     * 更新决策流
     *
     * @param flowId 决策流ID
     * @param graph 决策流图
     */
    void update(Long flowId, String graph);

    /**
     * 删除决策流
     *
     * @param flowId 决策流ID
     * @return 是否成功
     */
    boolean delete(Long flowId);

    /**
     * 新增决策流版本
     *
     * @param flowId 决策流ID
     * @return 响应参数
     */
    FlowResp newVersion(Long flowId);

    /**
     * 查询分类下决策流列表
     *
     * @param param 分类ID
     * @return 响应参数
     */
    PageResult<FlowResp> queryPage(PageParam param);

    /**
     * 查询分类下决策流列表
     *
     * @param categoryId 分类ID
     * @return 响应参数
     */
    List<FlowResp> listByCategory(Long categoryId);

    /**
     * 查询分组下所有决策流
     *
     * @param groupId 分组ID
     * @return 响应参数
     */
    List<FlowResp> listByGroup(Long groupId);

    /**
     * 构建决策流
     * @param flowId 决策流ID
     * @return 决策流
     */
    Flow buildFlow(Long flowId);

    /**
     * 执行决策流
     *
     * @param flowId 流程id
     * @param param 参数
     * @return 响应参数
     */
    FlowResultResp execute(Long flowId, Map<String, Object> param);

    /**
     * 异步执行决策流
     *
     * @param flowId 流程id
     * @param param 参数
     * @return 响应参数
     */
    String executeAsync(Long flowId, Map<String, Object> param);

    /**
     * 测试决策流
     *
     * @param req 请求参数
     * @return 响应参数
     */
    SseEmitter test(FlowExecReq req);


    /**
     * 恢复执行决策流
     *
     * @param req 参数
     * @return SseEmitter
     */
    SseEmitter resume(ResumeReq req);

    /**
     * 获取流程执行参数
     * @param flowId 流程Id
     * @return 执行参数
     */
    List<VariableResp> getFlowVars(Long flowId);
}
