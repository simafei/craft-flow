package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 对象字段表
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("variable")
public class VariablePO extends BasePO {


    /**
     * 流程分组Id
     */
    @TableField("group_id")
    private Long groupId;

    /**
     * 字段名称
     */
    @TableField("var_name")
    private String varName;

    /**
     * 字段中文名
     */
    @TableField("var_label")
    private String varLabel;

    /**
     * 字段类型 0: 字符, 1: 整数, 2:浮点数
     */
    @TableField("var_type")
    private Integer varType;

    /**
     * 数据作用域 0: 请求，1: 赋值
     */
    @TableField("field_scope")
    private Integer fieldScope;

    /**
     * 初始值
     */
    @TableField("init_value")
    private String initValue;
}
