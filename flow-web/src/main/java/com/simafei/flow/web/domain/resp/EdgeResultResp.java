package com.simafei.flow.web.domain.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengpengju
 * @since 2024-06-20
 */
@Getter
@Setter
public class EdgeResultResp {

    private static final long serialVersionUID = 1L;

    /**
     * 执行请求id
     */
    @Schema(description = "执行请求id")
    private String execId;

    /**
     * 执行规则id
     */
    @Schema(description = "执行规则id")
    private String flowId;

    /**
     * 执行连线Id
     */
    @Schema(description = "执行连线Id")
    private String edgeId;

    /**
     * 执行参数
     */
    @Schema(description = "执行参数")
    private String execParams;

    /**
     * 执行时间
     */
    @Schema(description = "执行时间")
    private Long spendTime;

    /**
     * 执行结果
     */
    @Schema(description = "执行结果")
    private Boolean pass;

    /**
     * 执行状态，0:未开始，1:执行中，2:执行完成
     */
    @Schema(description = "执行状态，0:未开始，1:执行中，2:执行完成")
    private Integer execStatus;
}
