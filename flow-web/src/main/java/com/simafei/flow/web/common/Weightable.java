package com.simafei.flow.web.common;

/**
 * @author fengpengju
 */
public interface Weightable {

    /**
     * 获取权重
     * @return 权重
     */
    int getWeight();
}
