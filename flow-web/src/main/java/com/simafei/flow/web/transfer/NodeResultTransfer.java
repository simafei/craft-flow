package com.simafei.flow.web.transfer;

import com.simafei.flow.core.NodeResult;
import com.simafei.flow.web.domain.resp.NodeResultResp;
import com.simafei.flow.web.entity.NodeResultPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface NodeResultTransfer {

    NodeResultTransfer INSTANCE = Mappers.getMapper(NodeResultTransfer.class);

    /**
     * entity转dto
     * @param nodeResult 节点执行结果
     * @return 节点执行结果
     */
    NodeResultResp toResp(NodeResultPO nodeResult);

    /**
     * entity转dto
     * @param nodeResults 节点执行结果
     * @return 节点执行结果
     */
    List<NodeResultResp> toResp(List<NodeResultPO> nodeResults);


    /**
     * entity转bo
     * @param result 节点执行结果
     * @return 节点
     */
    @Mapping(target = "inputParams", source = "execParams")
    @Mapping(target = "results", source = "execResult")
    NodeResult toBo(NodeResultPO result);


    /**
     * entity转bo
     * @param nodeResults 节点执行结果
     * @return 节点执行结果
     */
    List<NodeResult> toBo(List<NodeResultPO> nodeResults);
}
