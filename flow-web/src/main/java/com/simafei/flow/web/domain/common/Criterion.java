package com.simafei.flow.web.domain.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simafei.flow.core.common.CalcType;
import com.simafei.flow.core.common.Operator;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fengpengju
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Criterion implements Condition {
    @Schema(description = "条件类型(分组还是单个条件)", allowableValues = {"Criteria", "Criterion"})
    private String type;

    @Schema(description = "左值")
    @JsonProperty(required = true)
    private String left;

    @Schema(description = "操作符")
    @JsonProperty(required = true)
    private Operator operator;

    @Schema(description = "右值")
    private String right;

    @Schema(description = "右值类型")
    private CalcType rightType;
}
