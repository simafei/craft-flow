package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 决策流
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("flow")
public class FlowPO extends BasePO {


    /**
     * 前端传递的图JSON
     */
    @TableField("graph")
    private String graph;

    /**
     * 决策流名称
     */
    @TableField("flow_name")
    private String flowName;

    /**
     * 场景分类Id
     */
    @TableField("category_id")
    private Long categoryId;

    /**
     * 是否生效
     */
    @TableField("valid")
    private Integer valid;

    /**
     * 发布状态
     */
    @TableField("status")
    private Integer status;

    /**
     * 分组Id
     */
    @TableField("group_id")
    private Long groupId;

    /**
     * 权重
     */
    @TableField("weight")
    private Integer weight;

    /**
     * 版本号
     */
    @TableField("version_no")
    private String versionNo;
}
