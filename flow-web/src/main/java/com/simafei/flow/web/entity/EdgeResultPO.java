package com.simafei.flow.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.simafei.flow.web.common.BasePO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengpengju
 * @since 2024-07-19
 */
@Getter
@Setter
@TableName("edge_result")
public class EdgeResultPO extends BasePO {


    /**
     * 执行请求id
     */
    @TableField("exec_id")
    private String execId;

    /**
     * 执行规则id
     */
    @TableField("flow_id")
    private String flowId;

    /**
     * 执行连线Id
     */
    @TableField("edge_id")
    private String edgeId;

    /**
     * 执行参数
     */
    @TableField("exec_params")
    private String execParams;

    /**
     * 边条件过滤后参数
     */
    @TableField("filtered_params")
    private String filteredParams;

    /**
     * 执行时间
     */
    @TableField("spend_time")
    private Long spendTime;

    /**
     * 执行状态，0:未开始，1:执行中，2:执行完成
     */
    @TableField("exec_status")
    private Integer execStatus;

    /**
     * 是否通过
     */
    @TableField("pass")
    private Boolean pass;
}
