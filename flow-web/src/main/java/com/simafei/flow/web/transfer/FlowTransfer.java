package com.simafei.flow.web.transfer;

import com.simafei.flow.web.domain.req.FlowReq;
import com.simafei.flow.web.domain.resp.FlowResp;
import com.simafei.flow.web.entity.FlowPO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author fengpengju
 */
@Mapper(uses = TypeHandler.class)
public interface FlowTransfer {

    FlowTransfer INSTANCE = Mappers.getMapper(FlowTransfer.class);

    /**
     * entity转dto
     * @param flow 模型
     * @return 规则dto
     */
    FlowResp toResp(FlowPO flow);

    /**
     * entity转dto
     * @param req 模型
     * @return 规则dto
     */
    List<FlowResp> toResp(List<FlowPO> req);

    /**
     * dto转entity
     * @param req 规则dto
     * @return entity
     */
    FlowPO toPo(FlowReq req);
}
