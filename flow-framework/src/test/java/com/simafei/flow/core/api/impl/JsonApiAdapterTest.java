package com.simafei.flow.core.api.impl;

import com.simafei.flow.core.api.Api;
import com.simafei.flow.core.api.SerializeType;
import com.simafei.flow.core.exception.ApiCallException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.net.ssl.SSLSession;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JsonApiAdapterTest {

    @Mock
    private HttpClient httpClient;

    private JsonApiAdapter jsonApiAdapter;

    @Before
    public void setUp() {
        jsonApiAdapter = new JsonApiAdapter(httpClient);
    }

    @Test
    public void testExchangeWithSuccessResponse() throws IOException, InterruptedException {
        String responseBody = "{\"result\":\"success\"}";

        CompletableFuture<HttpResponse<String>> responseFuture = CompletableFuture.completedFuture(
                new HttpResponse<>() {
                    @Override
                    public int statusCode() {
                        return 200;
                    }

                    @Override
                    public HttpRequest request() {
                        return null;
                    }

                    @Override
                    public Optional<HttpResponse<String>> previousResponse() {
                        return Optional.empty();
                    }

                    @Override
                    public HttpHeaders headers() {
                        return null;
                    }

                    @Override
                    public String body() {
                        return responseBody;
                    }

                    @Override
                    public Optional<SSLSession> sslSession() {
                        return Optional.empty();
                    }

                    @Override
                    public URI uri() {
                        return null;
                    }

                    @Override
                    public HttpClient.Version version() {
                        return HttpClient.Version.HTTP_1_1;
                    }
                }
        );

        // Set up the mock HttpClient to return the mock HttpResponse
        when(httpClient.send(any(HttpRequest.class), eq(HttpResponse.BodyHandlers.ofString())))
                .thenReturn(responseFuture.join());

        Api api = Api.builder()
                .httpUrl("http://localhost:8080/mock?key=${key}")
                .serializeType(SerializeType.JSON)
                .httpMethod("GET")
                .build();
        Map<String, Object> params = Collections.singletonMap("key", "value");

        List<Map<String, Object>> result = jsonApiAdapter.exchange(api, params);

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertEquals("success", result.get(0).get("result"));
    }

    @Test(expected = ApiCallException.class)
    @SuppressWarnings("unchecked")
    public void testExchangeWithErrorResponse() throws IOException, InterruptedException {
        when(httpClient.send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class)))
                .thenThrow(new IOException("Network error"));

        Api api = Api.builder()
                .httpUrl("http://localhost:8080/mock?key=${key}")
                .serializeType(SerializeType.JSON)
                .httpMethod("GET")
                .build();
        Map<String, Object> params = Collections.singletonMap("key", "value");
        jsonApiAdapter.exchange(api, params);
    }
}
