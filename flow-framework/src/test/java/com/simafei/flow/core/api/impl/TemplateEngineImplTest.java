package com.simafei.flow.core.api.impl;

import com.simafei.flow.core.api.TemplateEngine;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TemplateEngineImplTest {

    private TemplateEngine templateEngine;
    private Map<String, Object> params;

    @Before
    public void setUp() {
        templateEngine = new TemplateEngineImpl();
        params = new HashMap<>();
    }

    @Test
    public void testRenderWithValidParams() {
        String template = "Hello, ${name}! Your score is ${score}.";
        params.put("name", "John");
        params.put("score", "90");

        String result = templateEngine.render(template, params);
        assertEquals("Hello, John! Your score is 90.", result);
    }

    @Test
    public void testRenderWithMissingParams() {
        String template = "Hello, ${name}! Your score is ${score}.";
        params.put("name", "John");

        String result = templateEngine.render(template, params);
        assertEquals("Hello, John! Your score is ${score}.", result);
    }

    @Test
    public void testRenderWithEmptyTemplate() {
        String template = "";

        String result = templateEngine.render(template, params);
        assertEquals("", result);
    }

    @Test
    public void testRenderWithNoPlaceholder() {
        String template = "Hello, world!";

        String result = templateEngine.render(template, params);
        assertEquals("Hello, world!", result);
    }

    @Test
    public void testRenderWithMultiplePlaceholders() {
        String template = "Hi ${name}, you have ${count} unread messages.";
        params.put("name", "Alice");
        params.put("count", "5");

        String result = templateEngine.render(template, params);
        assertEquals("Hi Alice, you have 5 unread messages.", result);
    }
}
