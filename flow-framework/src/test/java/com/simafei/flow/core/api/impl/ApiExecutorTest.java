package com.simafei.flow.core.api.impl;

import com.simafei.flow.core.ExecutionResult;
import com.simafei.flow.core.api.Api;
import com.simafei.flow.core.api.ApiAdapter;
import com.simafei.flow.core.api.SerializeType;
import com.simafei.flow.core.exception.ApiCallException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ApiExecutorTest {

    private ApiExecutor apiExecutor;

    private ApiAdapter apiAdapter;

    @Before
    public void setUp() {
        apiAdapter = Mockito.mock(ApiAdapter.class);
        apiExecutor = new ApiExecutor(List.of(apiAdapter));
        when(apiAdapter.serializeType()).thenReturn(SerializeType.JSON);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testExecuteWithSingleParam() {
        Api api = Api.builder()
                .httpUrl("http://localhost:8080/mock?key=${key}")
                .serializeType(SerializeType.JSON)
                .httpMethod("GET")
                .build();
        Map<String, Object> params = Collections.singletonMap("key", "value");
        when(apiAdapter.exchange(any(Api.class), any(Map.class)))
                .thenReturn(Collections.singletonList(Collections.singletonMap("result", "success")));

        ExecutionResult result = apiExecutor.execute(api, params);

        assertNotNull(result);
        assertEquals(1, result.getResults().size());
        assertEquals("success", result.getResults().get(0).get("result"));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testExecuteWithMultipleParams() {
        Api api = Api.builder().serializeType(SerializeType.JSON).build();
        List<Map<String, Object>> params = Collections.singletonList(Collections.singletonMap("key", "value"));
        when(apiAdapter.exchange(any(Api.class), any(Map.class)))
                .thenReturn(Collections.singletonList(Collections.singletonMap("result", "success")));

        List<ExecutionResult> results = apiExecutor.execute(api, params);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals("success", results.get(0).getResults().get(0).get("result"));
    }

    @Test(expected = ApiCallException.class)
    public void testExecuteWithUnsupportedSerializeType() {
        Api api = Api.builder().serializeType(SerializeType.XML).build();
        Map<String, Object> params = Collections.singletonMap("key", "value");

        apiExecutor.execute(api, params);
    }

    @Test
    public void testTemplate() {
        String template = "mock";
        String expected = "var mock = \"Hello, World\"";

        String result = apiExecutor.template(template);

        assertEquals(result, expected);
    }
}
