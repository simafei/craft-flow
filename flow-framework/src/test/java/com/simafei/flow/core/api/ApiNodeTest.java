package com.simafei.flow.core.api;

import com.simafei.flow.core.ExecutionContext;
import com.simafei.flow.core.ExecutionResult;
import com.simafei.flow.core.api.impl.ApiExecutor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ApiNodeTest {

    @Mock
    private ApiExecutor executor;

    @Mock
    private Api api;

    @InjectMocks
    private ApiNode apiNode;

    private static final String PARAM_KEY = "key";
    private static final String PARAM_VALUE = "value";

    @BeforeEach
    void setUp() {
        apiNode = new ApiNode();
        apiNode.setApi(api);
        apiNode.setExecutor(executor);
    }

    @Test
    @SuppressWarnings("unchecked")
    void testDoExecuteReturnsResults() {
        String expected = "success";
        // Arrange
        Map<String, Object> params = Collections.singletonMap(PARAM_KEY, PARAM_VALUE);
        List<ExecutionResult> expectedResults = Collections.singletonList(
                ExecutionResult.builder()
                        .inputParams(params)
                        .results(Collections.singletonList(Collections.singletonMap("result", expected)))
                        .build());

        when(executor.execute(any(Api.class), any(List.class))).thenReturn(expectedResults);

        List<ExecutionResult> results = executor.execute(api, Collections.singletonList(params));
        // Assert
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(expected, results.get(0).getResults().get(0).get("result"));
    }

    @Test
    @SuppressWarnings("unchecked")
    void testDoExecuteWithEmptyParamsReturnsEmptyResults() {
        // Arrange
        when(executor.execute(any(Api.class), any(List.class))).thenReturn(Collections.emptyList());

        // Act
        List<ExecutionResult> results = apiNode.doExecute(ExecutionContext.builder().build(), Collections.emptyList());

        // Assert
        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    void testDoExecutePassesCorrectApiAndParamsToExecutor() {
        // Arrange
        Map<String, Object> params = Collections.singletonMap(PARAM_KEY, PARAM_VALUE);
        List<ExecutionResult> expectedResults = Collections.singletonList(new ExecutionResult());
        when(executor.execute(api, Collections.singletonList(params))).thenReturn(expectedResults);

        // Act
        apiNode.doExecute(ExecutionContext.builder().build(), Collections.singletonList(params));

        // Assert
        verify(executor).execute(api, Collections.singletonList(params));
    }
}
