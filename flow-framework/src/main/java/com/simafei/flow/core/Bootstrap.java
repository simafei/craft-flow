package com.simafei.flow.core;

import cn.hutool.core.map.MapUtil;
import com.simafei.flow.core.api.Api;
import com.simafei.flow.core.api.SerializeType;
import com.simafei.flow.core.common.Assignment;
import com.simafei.flow.core.common.CalcType;
import com.simafei.flow.core.common.Criteria;
import com.simafei.flow.core.common.FieldType;
import com.simafei.flow.core.data.ds.FlowDataSource;
import com.simafei.flow.core.json.JsonUtils;
import com.simafei.flow.core.rule.FlowRule;
import com.simafei.flow.core.rule.FlowRules;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public class Bootstrap {

    public static void main(String[] args) {
        FlowFactory flowFactory = new FlowFactory(new FlowDataSource());

        Edge edge1 = new Edge();
        Edge edge2 = new Edge();
        Edge edge3 = new Edge();

        Criteria or = Criteria.create().eq("a", "1", CalcType.CONSTANT)
                .lt("b", "a", CalcType.VARIABLE)
                .or(Criteria.create()
                        .lt("c", "3", CalcType.CONSTANT)
                        .and(Criteria.create().eq("d", "4", CalcType.CONSTANT)));

        Criteria temp = Criteria.create().gt("id", "1", CalcType.CONSTANT);
        System.out.println(JsonUtils.toJsonString(or));

        Criteria criteria = JsonUtils.parseObject(JsonUtils.toJsonString(or), Criteria.class);
        System.out.println(criteria);

        Map<String, Object> map = MapUtil.newHashMap();
        map.put("a", 1);
        map.put("b", 3);
        map.put("c", 2);
        map.put("d", 4);
        System.out.println(or.toExpr(map));

        FlowRule build = FlowRule.builder()
                .ruleName("rule1")
                .description("rule1")
                .priority(1)
                .enabled(true)
                .assignments(List.of(Assignment.builder()
                        .left("a").right("100").rightType(CalcType.CONSTANT).rightFieldType(FieldType.INTEGER).build()))
                .criteria(or)
                .build();

        Node ruleNode = flowFactory.createRuleNode("rule1", "rule1", FlowRules.builder().flowRules(List.of(build)).build());

        Node apiNode = flowFactory.createApiNode("api1", "api1", Api.builder()
                .httpMethod("GET")
                .serializeType(SerializeType.JSON)
                .httpUrl("https://dc-dev1.mcisaas.com/decision/v3/api-docs").build());

        Node startNode = flowFactory.createStartNode("start", "start");

        Node endNode = flowFactory.createEndNode("end", "end", List.of());

        edge1.setFrom(startNode);
        edge1.setTo(ruleNode);

        edge2.setFrom(apiNode);
        edge2.setTo(ruleNode);

        edge3.setFrom(ruleNode);
        edge3.setTo(endNode);

        Flow flow = new Flow();
        flow.setEdges(List.of(edge1, edge2, edge3));
        flow.setNodes(List.of(startNode, apiNode, ruleNode, endNode));

        flow.executeAsync(FlowExecution.builder().build(), map);

        System.out.println("flow execute task is running");

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
