package com.simafei.flow.core.common;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public interface Condition {

    /**
     * 获取字段
     * @return 字段
     */
    List<String> fields();

    /**
     * 获取连接符
     * @return 连接符
     */
    Conj getConj();

    /**
     * 转换为表达式
     * @param paramMap 参数
     * @return 表达式
     */
    String toExpr(Map<String, Object> paramMap);

    /**
     * 转换为sql表达式
     * @param paramMap 参数
     * @return sql表达式
     */
    SqlConditionResult toSqlExpr(Map<String, Object> paramMap);


    @Data
    @Builder
    class SqlConditionResult {
        private String when;
        private List<String> args;
    }
}
