package com.simafei.flow.core.common;

import lombok.*;

/**
 * @author fengpengju
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Assignment {

    /**
     * 左边表达式
     */
    private String left;

    /**
     * 右边表达式
     */
    private String right;

    /**
     * 右边数据类型
     */
    private FieldType rightFieldType;

    /**
     * 右边表达式类型
     */
    private CalcType rightType;
}
