package com.simafei.flow.core.exception;

/**
 * @author fengpengju
 */
public class ApiCallException extends FlowException {

    public ApiCallException(String message) {
        super(message);
    }

    public ApiCallException(String message, Throwable cause) {
        super(message, cause);
    }
}
