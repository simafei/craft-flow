package com.simafei.flow.core;

import lombok.Builder;
import lombok.Getter;

/**
 * @author fengpengju
 */
@Builder
@Getter
public class FlowExecution {

    /**
     * 执行ID
     */
    private String execId;

    /**
     * 是否测试
     */
    private boolean test;

    /**
     * 执行监听器
     */
    @Builder.Default
    private ExecutionListener executionListener = new ExecutionListener() {};
}
