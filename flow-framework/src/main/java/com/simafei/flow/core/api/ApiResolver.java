package com.simafei.flow.core.api;

import java.util.List;
import java.util.Map;

/**
 * 第三方api解析器
 * @author fengpengju
 */
public interface ApiResolver {

    /**
     * 获取请求头
     * @param params 节点参数
     * @return 请求头
     */
    Map<String, String> headers(Map<String, Object> params);

    /**
     * 获取请求体
     * @param params 节点参数
     * @return 请求体
     */
    String body(Map<String, Object> params);

    /**
     * 解析请求体
     * @param response 响应体
     * @param params 节点参数
     * @return 请求url
     */
    List<Map<String, Object>> resolve(Map<String, Object> response, Map<String, Object> params);


    /**
     * 是否执行
     * @param params 节点参数
     * @return 是否执行
     */
    default boolean shouldExecute(Map<String, Object> params) {
        return true;
    }
}
