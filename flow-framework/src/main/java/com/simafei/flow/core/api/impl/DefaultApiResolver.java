package com.simafei.flow.core.api.impl;

import cn.hutool.core.map.MapUtil;
import com.simafei.flow.core.api.ApiResolver;
import com.simafei.flow.core.json.JsonUtils;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public class DefaultApiResolver implements ApiResolver {

    @Override
    public Map<String, String> headers(Map<String, Object> params) {
        // 构造请求头
        return MapUtil.newHashMap();
    }

    @Override
    public String body(Map<String, Object> params) {
        // 构造请求体JSON
        return JsonUtils.toJsonString(params);
    }

    @Override
    public List<Map<String, Object>> resolve(Map<String, Object> response, Map<String, Object> params) {
        // 请求响应解析
        return List.of(response);
    }

    @Override
    public boolean shouldExecute(Map<String, Object> params) {
        // 在这里判断模型需不需要执行
        return true;
    }
}
