package com.simafei.flow.core.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */
@RequiredArgsConstructor
@Getter
public enum Templates {

    // 默认模板
    DEFAULT("default", "系统通用API解析"),

    ;

    private final String name;

    private final String desc;
}
