package com.simafei.flow.core.rule.matcher;

import com.simafei.flow.core.common.Criteria;
import com.simafei.flow.core.common.ListOperator;
import com.simafei.flow.core.common.Predicate;
import com.simafei.flow.core.rule.ListMatcher;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public class AllMatcher implements ListMatcher {

    @Override
    public boolean match(Criteria criteria, List<Map<String, Object>> params) {
        return params.stream().allMatch(param -> Predicate.eval(criteria, param));
    }

    @Override
    public boolean support(ListOperator listOperator) {
        return ListOperator.ALL_MATCH == listOperator;
    }
}
