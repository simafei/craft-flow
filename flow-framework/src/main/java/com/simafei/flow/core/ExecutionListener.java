package com.simafei.flow.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fengpengju
 */
public interface ExecutionListener {

    Logger LOGGER = LoggerFactory.getLogger(ExecutionListener.class);

    /**
     * 执行前
     *
     * @param context 执行上下文
     * @param flow    执行流程
     */
    default void beforeFlowExecute(ExecutionContext context, Flow flow) {
        LOGGER.debug("beforeExecute: {}", flow);
    }

    /**
     * 执行后
     *
     * @param context 执行上下文
     * @param flow     执行流程
     * @param result  执行结果
     */
    default void afterFlowExecute(ExecutionContext context, Flow flow, FlowResult result) {
        LOGGER.debug("afterExecute: flow={}, result={}", flow, result);
    }

    /**
     * 异常处理
     * @param context 执行上下文
     * @param flow 流程
     * @param throwable 异常
     */
    default void onFlowError(ExecutionContext context, Flow flow, Throwable throwable) {
        LOGGER.error("flow id={}, execution error", flow.getId(), throwable);
    }

    /**
     * 执行前
     * @param context 执行上下文
     * @param node    执行节点
     */
    default void beforeNodeExecute(ExecutionContext context, Node node) {
        LOGGER.debug("beforeExecute: {}", node);
    }

    /**
     * 执行后
     *
     * @param context 执行上下文
     * @param node    执行节点
     * @param result  执行结果
     */
    default void afterNodeExecute(ExecutionContext context, Node node, NodeResult result) {
        LOGGER.debug("afterExecute: node={}, result={}", node, result);
    }

    /**
     * 执行前
     * @param context 执行上下文
     * @param edge    执行节点
     */
    default void beforeEdgeExecute(ExecutionContext context, Edge edge) {
        LOGGER.debug("beforeExecute: {}", edge);
    }

    /**
     * 执行后
     *
     * @param context 执行上下文
     * @param edge    执行节点
     * @param result  执行结果
     */
    default void afterEdgeExecute(ExecutionContext context, Edge edge, EdgeResult result) {
        LOGGER.debug("afterExecute: edge={}, result={}", edge, result);
    }
}
