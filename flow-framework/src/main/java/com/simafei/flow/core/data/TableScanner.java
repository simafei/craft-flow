package com.simafei.flow.core.data;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.simafei.flow.core.common.Condition;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
@RequiredArgsConstructor
public class TableScanner {

    private final static int BATCH_SIZE = 1000;

    private final JdbcTemplate jdbcTemplate;

    public void scan(ScanSpec config, ScanListener listener) {
        Object id = 0;
        if (config.getIdColumnType() != 0) {
            id = "";
        }
        String select = "*";
        // 默认按Id列遍历
        String idColumn = StrUtil.isEmpty(config.getIdColumn()) ? "id" : config.getIdColumn();
        if (CollectionUtil.isNotEmpty(config.getColumns())) {
            List<String> columns = new ArrayList<>(config.getColumns());
            columns.add(idColumn);
            select = StrUtil.join(",", columns);
        }
        Condition.SqlConditionResult result = config.getCriteria().toSqlExpr(MapUtil.newHashMap());
        String sql = new SQL()
                .SELECT(select)
                .FROM(config.getTableName())
                .WHERE(idColumn + ">? and " + result.getWhen())
                .ORDER_BY(idColumn)
                .LIMIT(BATCH_SIZE).toString();
        while (true) {
            List<Object> args = new LinkedList<>(result.getArgs());
            args.add(0, id);

            List<Map<String, Object>> records = jdbcTemplate.queryForList(sql, args.toArray());

            if (records.isEmpty()) {
                break;
            }

            listener.onRecords(records);
            id = records.get(records.size() - 1).getOrDefault(idColumn, 0);
        }
    }
}
