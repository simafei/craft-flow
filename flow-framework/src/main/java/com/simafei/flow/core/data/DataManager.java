package com.simafei.flow.core.data;

import com.simafei.flow.core.common.Variable;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public interface DataManager {

    /**
     * 加载数据
     * @param strategy 数据加载配置
     * @param params 参数
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> load(LoadSpec strategy, Map<String, Object> params);


    /**
     * 加载数据
     * @param query 数据加载配置
     * @param params 入参参数
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> aggregate(AggSpec query, Map<String, Object> params);


    /**
     * 保存或更新数据
     * @param storeSpec 数据存储配置
     * @param params 参数
     * @return List<Integer> 每一行执行影响的条数
     */
    List<Integer> saveOrUpdate(StoreSpec storeSpec, List<Map<String, Object>> params);


    /**
     * 获取数据源名称列表
     * @return List<String>
     */
    List<String> getDataSourceNames();

    /**
     * 添加数据源
     * @param key 数据源Key
     * @param dataSource 数据源
     */
    void add(String key, DataSource dataSource);

    /**
     * 移除数据源
     * @param key 数据源Key
     */
    void remove(String key);

    /**
     * 判断数据源是否存在
     * @param key 数据源Key
     * @return true/false
     */
    boolean exists(String key);

    /**
     * 获取数据源
     * @param dataSource 数据源Key
     * @return 该数据源下的表名
     */
    List<String> tables(String dataSource);

    /**
     * 获取数据源下的表字段
     * @param dataSource 数据源Key
     * @param database 数据库名
     * @param tableName 表名
     * @return 该数据源下的表字段
     */
    List<Variable> columns(String dataSource, String database, String tableName);
}
