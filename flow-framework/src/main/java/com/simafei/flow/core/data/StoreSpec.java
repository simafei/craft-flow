package com.simafei.flow.core.data;

import com.simafei.flow.core.common.Criteria;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author fengpengju
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StoreSpec {

    /**
     * 数据源
     */
    private String dataSource;

    /**
     * 数据库表名
     */
    private String tableName;

    /**
     * 字段映射
     */
    private Map<String, String> mapping;

    /**
     * 条件
     */
    private Criteria criteria;
}
