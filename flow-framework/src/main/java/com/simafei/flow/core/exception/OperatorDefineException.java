package com.simafei.flow.core.exception;

/**
 * @author fengpengju
 */
public class OperatorDefineException extends FlowException {

    public OperatorDefineException(String message) {
        super(message);
    }
}
