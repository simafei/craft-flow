package com.simafei.flow.core.rule;

import org.jeasy.rules.api.Facts;

/**
 * @author fengpengju
 */
public interface Action {

    /**
     * 执行方法
     * @param facts facts
     */
    void eval(Facts facts);
}
