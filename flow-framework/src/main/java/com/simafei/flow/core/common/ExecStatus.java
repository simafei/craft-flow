package com.simafei.flow.core.common;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */
@RequiredArgsConstructor
@Getter
public enum ExecStatus {

    // 0-初始化 1-等待 2-运行中 3-拒绝 4-通过
    INIT(0, "初始化"),

    WAITING(1, "等待中"),

    RUNNING(2, "运行中"),

    FAILED(3, "异常"),

    FINISHED(4, "已完成"),

    INTERRUPT(5, "中断"),
    ;
    private final int value;

    private final String desc;

    public static ExecStatus of(int value) {
        for (ExecStatus status : ExecStatus.values()) {
            if (status.value == value) {
                return status;
            }
        }
        return ExecStatus.INIT;
    }
}
