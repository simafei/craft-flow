package com.simafei.flow.core.exception;

/**
 * @author fengpengju
 */
public class EnumValueException extends FlowException {

    public EnumValueException() {
        super("枚举值不合法");
    }
}
