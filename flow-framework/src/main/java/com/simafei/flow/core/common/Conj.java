package com.simafei.flow.core.common;

import com.simafei.flow.core.exception.EnumValueException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */

@RequiredArgsConstructor
@Getter
public enum Conj {

    // 空格
    SPACE(" ", "空格"),

    // 并且
    AND("&&", "并且"),

    // 或者
    OR("||", "或者");

    private final String value;

    private final String desc;

    public static Conj of(String value) {
        for (Conj conj : Conj.values()) {
            if (conj.getValue().equals(value)) {
                return conj;
            }
        }
        throw new EnumValueException();
    }
}
