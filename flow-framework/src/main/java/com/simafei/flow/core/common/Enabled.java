package com.simafei.flow.core.common;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

/**
 * @author fengpengju
 */
@RequiredArgsConstructor
@Getter
public enum Enabled {

    /**
     * 启用
     */
    ENABLED(1),
    /**
     * 禁用
     */
    DISABLED(0);

    private final int value;

    public static boolean isEnabled(Integer value) {
        return Objects.equals(ENABLED.getValue(), value);
    }

    public static boolean isDisabled(Integer value) {
        return Objects.equals(DISABLED.getValue(), value);
    }
}
