package com.simafei.flow.core.common;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */
@RequiredArgsConstructor
@Getter
public enum ListOperator {

    // 任意一个满足
    ANY_MATCH("AnyMatch", "任意一个满足"),

    // 全部满足
    ALL_MATCH("AllMatch", "全部满足"),

    // 平均值
    NONE_MATCH("NoneMatch", "全部不满足"),

    // 超过一半满足
    HALF_MATCH("HalfMatch", "超过一半满足"),

    // 超过1/3满足
    ONE_THIRD_MATCH("OneThirdMatch", "超过1/3满足");

    private final String operator;

    private final String desc;

    public static ListOperator of(String operator) {
        if (operator == null) {
            return null;
        }
        for (ListOperator listOperator : ListOperator.values()) {
            if (listOperator.getOperator().equals(operator)) {
                return listOperator;
            }
        }
        throw new IllegalArgumentException("Unknown ListOperator: " + operator);
    }
}
