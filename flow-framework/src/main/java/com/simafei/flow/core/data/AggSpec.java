package com.simafei.flow.core.data;

import com.simafei.flow.core.common.Criteria;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author fengpengju
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AggSpec {

    /**
     * 数据源
     */
    private String dataSource;

    /**
     * 数据源表名
     */
    private String tableName;

    /**
     * 过滤条件
     */
    private Criteria criteria;

    /**
     * 聚合表别名
     */
    private List<AggColumn> aggColumns;

    /**
     * 分组字段
     */
    private List<String> groupBy;
}
