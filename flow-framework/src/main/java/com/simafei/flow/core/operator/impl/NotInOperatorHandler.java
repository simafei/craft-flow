package com.simafei.flow.core.operator.impl;

import com.simafei.flow.core.common.Criterion;
import com.simafei.flow.core.common.Operator;

/**
 * @author fengpengju
 */
public class NotInOperatorHandler extends InOperatorHandler {

    @Override
    public String handle(Criterion condition) {
        return String.format("!(%s)", super.handle(condition));
    }

    @Override
    public boolean support(Operator operator) {
        return Operator.NOT_IN == operator;
    }
}
