package com.simafei.flow.core.func;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author fengpengju
 * 枚举实现单例模式
 */
public enum Funcs {

    /**
     * 单例
     */
    INSTANCE;


    public int randInt(int max) {
        return ThreadLocalRandom.current().nextInt(max);
    }

    public <T extends Comparable<T>> T max(T a, T b) {
        if (a == null) {
            return b;
        }
        if (b == null) {
            return a;
        }
        if (a.compareTo(b) > 0) {
            return a;
        } else {
            return b;
        }
    }

    public <T extends Comparable<T>> T min(T a, T b) {
        if (a == null) {
            return b;
        }
        if (b == null) {
            return a;
        }
        if (a.compareTo(b) > 0) {
            return b;
        } else {
            return a;
        }
    }

    public static String date() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public static String time(String pattern) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    public static long dayOffset(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return ChronoUnit.DAYS.between(LocalDate.now(), localDate);
    }

    public static List<String> funcList() {
        return List.of("F.randInt(bound)", "F.max(a,b)", "F.min(a,b)", "F.date()", "F.time(yyyy-MM-dd HH:mm:ss)", "F.dayOffset(yyyy-MM-dd)");
    }


    public static void main(String[] args) {
        System.out.println(Funcs.INSTANCE.randInt(100));
        System.out.println(Funcs.INSTANCE.max(1, 2));
        System.out.println(Funcs.INSTANCE.min(1, 2));
        System.out.println(Funcs.date());
        System.out.println(Funcs.time("yyyy-MM-dd HH:mm:ss"));
        System.out.println(Funcs.dayOffset("2024-08-01"));
        System.out.println(Funcs.funcList());
    }
}
