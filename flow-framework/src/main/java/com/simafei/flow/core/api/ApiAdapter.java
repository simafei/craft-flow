package com.simafei.flow.core.api;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public interface ApiAdapter {

    /**
     * 交换数据
     * @param api api
     * @param params 参数
     * @return 交换结果
     */
    List<Map<String, Object>> exchange(Api api, Map<String, Object> params);

    /**
     * 序列化类型
     * @return 序列化类型
     */
    SerializeType serializeType();
}
