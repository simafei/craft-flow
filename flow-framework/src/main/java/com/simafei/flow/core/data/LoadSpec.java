package com.simafei.flow.core.data;

import com.simafei.flow.core.common.Criteria;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * @author fengpengju
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoadSpec {

    /**
     * 数据源
     */
    private String dataSource;

    /**
     * 数据表名
     */
    private String tableName;

    /**
     * 查询列
     */
    private Set<String> columns;

    /**
     * 查询条件
     */
    private Criteria criteria;
}
