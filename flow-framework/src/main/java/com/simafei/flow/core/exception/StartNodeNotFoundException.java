package com.simafei.flow.core.exception;

/**
 * @author fengpengju
 */
public class StartNodeNotFoundException extends FlowException {

    public StartNodeNotFoundException() {
        super("Start node not found");
    }
}
