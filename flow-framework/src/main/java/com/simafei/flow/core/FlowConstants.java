package com.simafei.flow.core;

import com.simafei.flow.core.common.FieldType;
import com.simafei.flow.core.common.FlowStatus;
import com.simafei.flow.core.common.Option;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author fengpengju
 */
public interface FlowConstants {

    String HAS_RESULT = "_has_result";

    String FUNC = "F";

    String RULE_NAME_COLUMN = "_rule_name";

    String RULE_RESULTS = "_rule_result";

    String PARAMS = "_params";

    String STATUS = "_status";

    String NULL = "null";

    String PASS = "通过";

    String REJECT = "拒绝";

    String YES = "true";

    String NO = "false";

    @RequiredArgsConstructor
    @Getter
    enum SystemVars {
        /**
         * 是否存在结果
         */
        HAS_RESULT(FlowConstants.HAS_RESULT, "是否存在结果", FieldType.ENUM,
                List.of(new Option<>(FlowConstants.YES, "是"), new Option<>(FlowConstants.NO, "否"))),

        DECISION_STATUS(FlowConstants.STATUS, "决策结果", FieldType.ENUM,
                List.of(new Option<>(FlowStatus.PASS.name(), FlowConstants.PASS), new Option<>(FlowStatus.REJECT.name(), FlowConstants.REJECT))),
        ;

        private final String varName;
        private final String varLabel;
        private final FieldType type;
        private final List<Option<String, String>> options;
    }
}
