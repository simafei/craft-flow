package com.simafei.flow.core.common;

import com.simafei.flow.core.exception.EnumValueException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 * 计算类型
 */
@RequiredArgsConstructor
@Getter
public enum CalcType {

    // 常量
    CONSTANT(0, "常量"),

    // 变量
    VARIABLE(1, "变量"),

    // 表达式
    EXPRESSION(2, "表达式")
    ;

    private final int value;

    private final String desc;

    public static CalcType of(int value) {
        for (CalcType calcType : CalcType.values()) {
            if (calcType.getValue() == value) {
                return calcType;
            }
        }
        throw new EnumValueException();
    }
}
