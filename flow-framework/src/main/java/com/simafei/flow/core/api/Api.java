package com.simafei.flow.core.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
*
* 算法模型表
* @author Generator
* @since 2024-01-22
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Api implements Serializable {

    /**
     * 使用的解析模板
     */
    private String template;

    /**
     * Api调用地址
     */
    private String httpUrl;

    /**
     * HTTP调用方法
     */
    private String httpMethod;

    /**
     * 组装请求参数和Header的脚本
     */
    private String serializeScript;

    /**
     * 序列化类型，JSON, XML
     */
    private SerializeType serializeType;
}

