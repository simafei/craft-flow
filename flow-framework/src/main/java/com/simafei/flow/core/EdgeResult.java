package com.simafei.flow.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EdgeResult {

    /**
     * 节点Id
     */
    private String edgeId;

    /**
     * 是否成功
     */
    private boolean pass;

    /**
     * 执行时间
     */
    private long spendTime;

    /**
     * 节点输入参数
     */
    private List<Map<String, Object>> inputParams;

    /**
     * 满足连线条件输入参数
     */
    private List<Map<String, Object>> filteredParams;
}
