package com.simafei.flow.core.common;

import com.simafei.flow.core.operator.OperatorHandler;
import com.simafei.flow.core.operator.impl.OperatorHandlerFactory;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
@Data
@AllArgsConstructor
public class Criterion implements Condition {

    private String left;

    private Operator operator;

    private String right;

    private CalcType rightType;

    @Override
    public List<String> fields() {
        return List.of(left);
    }

    @Override
    public Conj getConj() {
        return Conj.AND;
    }

    @Override
    public String toExpr(Map<String, Object> paramMap) {
        OperatorHandler operatorHandler = OperatorHandlerFactory.INSTANCE.getOperatorHandler(this.operator);
        operatorHandler.check(this);
        // 生成表达式，如："age" > 18
        return operatorHandler.handle(this);
    }

    @Override
    public SqlConditionResult toSqlExpr(Map<String, Object> paramMap) {
        // 生成表达式，如："age" > 18
        // 删掉了函数的处理，函数使用MVEL表达式的函数功能足够
        OperatorHandler operatorHandler = OperatorHandlerFactory.INSTANCE.getOperatorHandler(operator);
        return SqlConditionResult.builder()
                .when(operatorHandler.sqlLeft(this))
                .args(operatorHandler.sqlRight(this, paramMap))
                .build();
    }
}
