package com.simafei.flow.core.operator.impl;

import com.simafei.flow.core.FlowConstants;
import com.simafei.flow.core.common.Criterion;
import com.simafei.flow.core.common.Operator;
import com.simafei.flow.core.operator.OperatorHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public class IsNotEmptyOperatorHandler implements OperatorHandler {

    @Override
    public String handle(Criterion condition) {
        return condition.getLeft() + " != " + FlowConstants.NULL;
    }

    @Override
    public String sqlLeft(Criterion condition) {
        return String.format("(%s is not null or %s != '')", condition.getLeft(), condition.getLeft());
    }

    @Override
    public List<String> sqlRight(Criterion condition, Map<String, Object> params) {
        return new ArrayList<>();
    }

    @Override
    public boolean support(Operator operator) {
        return operator == Operator.IS_NOT_EMPTY;
    }
}
