package com.simafei.flow.core.data;

import com.simafei.flow.core.ExecutionContext;
import com.simafei.flow.core.ExecutionResult;
import com.simafei.flow.core.Node;
import com.simafei.flow.core.common.NodeType;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
@EqualsAndHashCode(callSuper = true)
@ToString
@Setter
public class AggNode extends Node {

    private AggSpec aggSpec;

    private DataManager dataManager;

    public AggNode() {
        super(NodeType.DATA_AGG);
    }

    @Override
    protected List<ExecutionResult> doExecute(ExecutionContext context, List<Map<String, Object>> params) {
        List<ExecutionResult> results = new ArrayList<>();
        for (Map<String, Object> param : params) {
            List<Map<String, Object>> dataMap = dataManager.aggregate(aggSpec, param);
            results.add(ExecutionResult.builder()
                    .inputParams(param)
                    .results(dataMap)
                    .build());
        }
        return results;
    }
}
