package com.simafei.flow.core.common;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */
@RequiredArgsConstructor
@Getter
public enum FlowStatus {

    // 0-初始化 1-通过 -1-拒绝
    REJECT(-1, "拒绝"),

    UNDETERMINED(0, "待定"),

    PASS(1, "通过"),
    ;
    private final int value;

    private final String desc;

    public static FlowStatus of(int value) {
        for (FlowStatus status : FlowStatus.values()) {
            if (status.value == value) {
                return status;
            }
        }
        return FlowStatus.UNDETERMINED;
    }
}
