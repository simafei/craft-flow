package com.simafei.flow.core.json;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * LocalDate反序列化规则
 * <p>
 *
 * @author huxuanguang
 */
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

    public static final LocalDateDeserializer INSTANCE = new LocalDateDeserializer();

    @Override
    public LocalDate deserialize(JsonParser p, DeserializationContext ctx) throws IOException, JsonProcessingException {
        String localdateString = p.readValueAs(String.class);
        if (StrUtil.isBlank(localdateString)) {
            return null;
        } else {
            return LocalDate.parse(localdateString, DateTimeFormatter.ISO_LOCAL_DATE);
        }
    }
}
