package com.simafei.flow.core.rule;

import com.simafei.flow.core.ExecutionContext;
import com.simafei.flow.core.ExecutionResult;
import com.simafei.flow.core.Node;
import com.simafei.flow.core.common.NodeType;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;

/**
 * 规则节点
 * @author fengpengju
 */
@EqualsAndHashCode(callSuper = true)
@ToString
@Setter
public class RuleNode extends Node {

    private FlowRules rules;

    private RuleExecutor executor;

    public RuleNode() {
        super(NodeType.RULE);
    }

    @Override
    protected List<ExecutionResult> doExecute(ExecutionContext context, List<Map<String, Object>> params) {
        return executor.execute(rules, params);
    }
}
