package com.simafei.flow.core.rule;

import com.simafei.flow.core.common.CalcType;
import com.simafei.flow.core.common.ExpressionExecutor;
import com.simafei.flow.core.common.FieldType;

import java.util.Map;

/**
 * @author fengpengju
 */
public class ValueHandler {

    public static Object handle(String expression, FieldType fieldType, CalcType calcType, Map<String, Object> param) {
        return switch (calcType) {
            case CONSTANT -> switch (fieldType) {
                case DECIMAL -> Double.parseDouble(expression);
                case INTEGER -> Long.parseLong(expression);
                case BOOLEAN -> Boolean.parseBoolean(expression);
                default -> expression;
            };
            case VARIABLE, EXPRESSION -> ExpressionExecutor.execute(expression, param);
        };
    }
}
