package com.simafei.flow.core.data;

import com.simafei.flow.core.ExecutionContext;
import com.simafei.flow.core.ExecutionResult;
import com.simafei.flow.core.Node;
import com.simafei.flow.core.common.NodeType;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
@EqualsAndHashCode(callSuper = true)
@ToString
@Setter
public class DataNode extends Node {

    private LoadSpec loadSpec;

    private DataManager dataManager;

    public DataNode() {
        super(NodeType.DATA_LOAD);
    }

    @Override
    protected List<ExecutionResult> doExecute(ExecutionContext context, List<Map<String, Object>> params) {
        List<ExecutionResult> results = new ArrayList<>();
        for (Map<String, Object> param : params) {
            List<Map<String, Object>> dataMap = dataManager.load(loadSpec, param);
            results.add(ExecutionResult.builder()
                    .inputParams(param)
                    .results(dataMap)
                    .build());
        }
        return results;
    }
}
