package com.simafei.flow.core.data;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public interface ScanListener {

    /**
     * 监听数据
     *
     * @param records 记录
     */
    void onRecords(List<Map<String, Object>> records);
}
