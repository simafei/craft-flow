package com.simafei.flow.core.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */
@RequiredArgsConstructor
@Getter
public enum AggFunc {

    // 求和
    SUM("SUM", "求和"),

    // 计数
    COUNT("COUNT", "计数"),

    // 平均值
    AVG("AVG", "平均值"),

    // 最大值
    MAX("MAX", "最大值"),

    // 最小值
    MIN("MIN", "最小值");

    private final String func;

    private final String desc;
}
