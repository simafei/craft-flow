package com.simafei.flow.core.operator.impl;


import com.simafei.flow.core.common.Criterion;
import com.simafei.flow.core.common.Operator;

/**
 * @author fengpengju
 */
public class NotLikeOperatorHandler extends LikeOperatorHandler {

    @Override
    public String handle(Criterion condition) {
        return "!" + super.handle(condition);
    }

    @Override
    public boolean support(Operator operator) {
        return Operator.NOT_LIKE == operator;
    }
}
