package com.simafei.flow.core.api;

import com.simafei.flow.core.ExecutionContext;
import com.simafei.flow.core.ExecutionResult;
import com.simafei.flow.core.Node;
import com.simafei.flow.core.api.impl.ApiExecutor;
import com.simafei.flow.core.common.NodeType;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;

/**
 * API节点
 * @author fengpengju
 */
@EqualsAndHashCode(callSuper = true)
@ToString
@Setter
public class ApiNode extends Node {

    private Api api;

    private ApiExecutor executor;

    public ApiNode() {
        super(NodeType.API);
    }

    @Override
    protected List<ExecutionResult> doExecute(ExecutionContext context, List<Map<String, Object>> params) {
        return executor.execute(api, params);
    }
}
