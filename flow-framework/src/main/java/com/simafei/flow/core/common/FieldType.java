package com.simafei.flow.core.common;

import com.simafei.flow.core.exception.EnumValueException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */

@RequiredArgsConstructor
@Getter
public enum FieldType {

    // String
    STRING(0, "文本", true),

    // Integer
    INTEGER(1, "整数", true),

    // DECIMAL
    DECIMAL(2, "小数", true),

    // Date
    DATE_TIME(3, "时间", true),

    // Boolean
    BOOLEAN(4, "布尔", true),

    // 对象
    OBJECT(5, "对象", false),

    // 数组
    LIST(6, "数组", false),

    // Date
    DATE(7, "日期", true),

    // 枚举
    ENUM(8, "枚举", true)
    ;

    private final int value;

    private final String desc;

    private final boolean enabled;

    public static FieldType of(int value) {
        for (FieldType fieldType : FieldType.values()) {
            if (fieldType.getValue() == value) {
                return fieldType;
            }
        }
        throw new EnumValueException();
    }
}
