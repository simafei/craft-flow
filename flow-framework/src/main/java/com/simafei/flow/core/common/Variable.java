package com.simafei.flow.core.common;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
*
* 对象字段表
* @author Generator
* @since 2024-01-02
*/
@Data
public class Variable implements Serializable {


    /**
     * 字段名称
     */
    private String varName;

    /**
     * 字段中文名
     */
    private String varLabel;

    /**
     * 字段类型 0: 字符, 1: 整数, 2:浮点数
     */
    private FieldType varType;

    /**
     * 数据作用域 0: 请求，1: 赋值
     */
    private FieldScope scope;

    /**
     * 初始值
     */
    private String initValue;

    /**
     * 枚举选项
     */
    private List<Option<String, String>> options;
}

