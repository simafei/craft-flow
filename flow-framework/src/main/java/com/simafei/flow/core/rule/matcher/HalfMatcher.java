package com.simafei.flow.core.rule.matcher;

import com.simafei.flow.core.common.Criteria;
import com.simafei.flow.core.common.ListOperator;
import com.simafei.flow.core.common.Predicate;
import com.simafei.flow.core.rule.ListMatcher;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public class HalfMatcher implements ListMatcher {
    @Override
    public boolean match(Criteria criteria, List<Map<String, Object>> params) {
        long count = params.stream().filter(param -> Predicate.eval(criteria, param)).count();

        return count * getMultiple() >= params.size();
    }

    @Override
    public boolean support(ListOperator operator) {
        return ListOperator.HALF_MATCH == operator;
    }

    protected int getMultiple() {
        return 2;
    }
}
