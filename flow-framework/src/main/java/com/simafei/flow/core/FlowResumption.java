package com.simafei.flow.core;

import cn.hutool.core.map.MapUtil;
import com.simafei.flow.core.common.FlowStatus;
import lombok.Builder;
import lombok.Getter;

import java.util.Map;
import java.util.Set;

/**
 * @author fengpengju
 */
@Builder
@Getter
public class FlowResumption {

    /**
     * 决策流ID
     */
    private String flowId;

    /**
     * 执行ID
     */
    private String execId;

    /**
     * 父节点ID
     */
    private String parentId;

    /**
     * 是否测试
     */
    private boolean test;

    /**
     * 当前要恢复的执行节点
     */
    private Node node;

    /**
     * 决策流执行入参
     */
    private Map<String, Object> flowInputParams;

    /**
     * 流程状态
     */
    private volatile FlowStatus flowStatus;

    /**
     * 已经执行节点执行结果
     */
    private Set<NodeResult> executedNodeResults;

    /**
     * 已经执行边执行结果
     */
    private Set<EdgeResult> executedEdgeResults;

    @Builder.Default
    private Map<String, Object> resumeParams = MapUtil.newHashMap();

    /**
     * 执行监听器
     */
    @Builder.Default
    private ExecutionListener executionListener = new ExecutionListener() {};
}
