package com.simafei.flow.core;

import com.simafei.flow.core.common.ExecStatus;
import com.simafei.flow.core.common.FlowStatus;
import com.simafei.flow.core.common.NodeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NodeResult {

    /**
     * 节点Id
     */
    private String nodeId;

    /**
     * 节点名称
     */
    private String name;

    /**
     * 节点类型
     */
    private NodeType nodeType;

    /**
     * 父节点执行Id
     */
    private String parentId;

    /**
     * 是否成功
     */
    private boolean success;

    /**
     * 异常信息
     */
    private Throwable cause;

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 结束时间
     */
    private Long endTime;

    /**
     * 节点输入参数
     */
    private List<Map<String, Object>> inputParams;

    /**
     * 节点执行结果
     */
    private List<ExecutionResult> results;

    /**
     * 执行状态
     */
    private ExecStatus execStatus;

    /**
     * 决策结果 -1-拒绝 0-待定 1-通过
     */
    private FlowStatus flowStatus;
}
