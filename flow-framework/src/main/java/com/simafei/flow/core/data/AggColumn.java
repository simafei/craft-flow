package com.simafei.flow.core.data;

import lombok.Data;

/**
 * @author fengpengju
 */
@Data
public class AggColumn {

    /**
     * 聚合表别名
     */
    private String alias;

    /**
     * 聚合列
     */
    private String column;

    /**
     * 聚合函数
     */
    private AggFunc aggFunc;
}
