package com.simafei.flow.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExecutionResult {

    /**
     * 参数列表
     */
    private Map<String, Object> inputParams;

    /**
     * 参数列表
     */
    private List<Map<String, Object>> results;
}
