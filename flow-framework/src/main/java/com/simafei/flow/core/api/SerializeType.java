package com.simafei.flow.core.api;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */

@RequiredArgsConstructor
@Getter
public enum SerializeType {

    // JSON
    JSON(0),

    // XML
    XML(1);

    private final int type;

    public static SerializeType of(int type) {
        for (SerializeType serializeType : SerializeType.values()) {
            if (serializeType.getType() == type) {
                return serializeType;
            }
        }
        throw new IllegalArgumentException("serializeType type is not exist");
    }
}
