package com.simafei.flow.core.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fengpengju
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Option<K, V> {

    private K value;

    private V label;
}
