package com.simafei.flow.core.rule;

import com.simafei.flow.core.common.Assignment;
import com.simafei.flow.core.common.Criteria;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
*
* 规则表
* @author Generator
* @since 2023-12-19
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlowRule implements Serializable {

    /**
     * 规则名称
     */
    private String ruleName;

    /**
     * 规则描述
     */
    private String description;

    /**
     * 规则条件
     */
    private Criteria criteria;

    /**
     * 赋值
     */
    private List<Assignment> assignments;

    /**
     * 优先级
     */
    private Integer priority;

    /**
     * 是否启用
     */
    private Boolean enabled;

}

