package com.simafei.flow.core.exception;

/**
 * @author fengpengju
 */
public class ConditionDefineException extends FlowException {

    public ConditionDefineException(String message) {
        super(message);
    }

}
