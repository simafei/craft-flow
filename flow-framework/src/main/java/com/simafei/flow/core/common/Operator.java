package com.simafei.flow.core.common;

import com.simafei.flow.core.exception.EnumValueException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */

@RequiredArgsConstructor
@Getter
public enum Operator {

    // 大于
    EQ("==", "=", "等于"),
    GT(">", ">", "大于"),
    GE(">=", ">=", "大于等于"),
    LT("<", "<", "小于"),
    LE("<=", "<=", "小于等于"),
    NEQ("!=", "!=", "不等于"),
    IN("contains", "in", "等于任意一个"),
    NOT_IN("", "not in", "不等于任意一个"),
    LIKE("contains", "like", "包含"),
    NOT_LIKE("", "not like", "不包含"),
    IS_EMPTY("", "", "为空"),
    IS_NOT_EMPTY("", "", "不为空"),
    ;

    private final String operator;

    private final String sqlOperator;

    private final String desc;

    public static Operator of(String operator) {
        for (Operator op : Operator.values()) {
            if (op.getOperator().equals(operator)) {
                return op;
            }
        }
        throw new EnumValueException();
    }
}
