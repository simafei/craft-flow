package com.simafei.flow.core.api;

import java.util.Map;

/**
 * @author fengpengju
 */
public interface TemplateEngine {

    /**
     * 渲染模板
     * @param template 模板
     * @param params 参数
     * @return 渲染结果
     */
    String render(String template, Map<String, Object> params);
}
