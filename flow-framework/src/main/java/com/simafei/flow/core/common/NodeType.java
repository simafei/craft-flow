package com.simafei.flow.core.common;

/**
 * @author fengpengju
 */

public enum NodeType {

    // 开始
    START,

    // 结束
    END,

    // 分支节点
    BRANCH,

    // 此处预留一些系统节点

    // 数据加载
    DATA_LOAD,

    // 数据聚合
    DATA_AGG,

    // 特征节点
    FEATURE,

    // 模型节点
    API,

    // 规则，赋值是特殊的规则，没必要单独开发赋值节点
    RULE,

    // 人工审核节点
    MANUAL,

    // 数据修改
    DATA_MODIFY,
}
