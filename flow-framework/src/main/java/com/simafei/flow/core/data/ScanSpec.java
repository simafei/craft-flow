package com.simafei.flow.core.data;

import com.simafei.flow.core.common.Criteria;
import lombok.Data;

import java.util.Set;

/**
 * @author fengpengju
 */
@Data
public class ScanSpec {
    /**
     * 数据源
     */
    private String dataSource;

    /**
     * 数据表名
     */
    private String tableName;

    /**
     * id列名
     */
    private String idColumn;

    /**
     * id列类型，0：整数，1：字符串
     */
    private Integer idColumnType;

    /**
     * 查询列
     */
    private Set<String> columns;

    /**
     * 查询条件
     */
    private Criteria criteria;
}
