package com.simafei.flow.core.rule;

import com.simafei.flow.core.common.Criteria;
import com.simafei.flow.core.common.ListOperator;

import java.util.List;
import java.util.Map;

/**
 * @author fengpengju
 */
public interface ListMatcher {

    /**
     * 匹配
     * @param criteria 条件
     * @param params 参数
     * @return 是否匹配
     */
    boolean match(Criteria criteria, List<Map<String, Object>> params);

    /**
     * 支持
     * @param operator 数值操作
     * @return 是否支持
     */
    boolean support(ListOperator operator);
}
