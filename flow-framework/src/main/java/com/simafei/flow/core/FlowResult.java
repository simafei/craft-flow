package com.simafei.flow.core;

import com.simafei.flow.core.common.ExecStatus;
import com.simafei.flow.core.common.FlowStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
*
* 
* @author Generator
* @since 2024-01-15
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlowResult implements Serializable {

    /**
     * 执行参数
     */
    private Map<String, Object> execParam;

    /**
     * 执行结果
     */
    private List<Map<String, Object>> execResults;

    /**
     * 执行状态
     */
    private ExecStatus execStatus;

    /**
     * 异常信息
     */
    private List<Throwable> causes;

    /**
     * 执行时间
     */
    private long spendTime;

    /**
     * 执行模式，0: 自动决策，1: 人工干预
     */
    private int execMode;

    /**
     * 决策结果，0: 待定，1: 通过，-1: 拒绝
     */
    private FlowStatus flowStatus;
}

