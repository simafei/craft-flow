package com.simafei.flow.core.rule;

import com.simafei.flow.core.common.ListOperator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author fengpengju
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FlowRules {

    /**
     * 是否开启
     */
    private boolean enableListOperator;

    /**
     * 列表操作
     */
    private ListOperator listOperator;

    /**
     * 规则集
     */
    private List<FlowRule> flowRules;
}
