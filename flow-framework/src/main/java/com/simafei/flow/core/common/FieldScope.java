package com.simafei.flow.core.common;

import com.simafei.flow.core.exception.EnumValueException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author fengpengju
 */

@RequiredArgsConstructor
@Getter
public enum FieldScope {

    // 请求
    REQUEST(0, "请求", true),

    // 响应
    RESPONSE(1, "响应", true),
    ;

    private final int value;

    private final String desc;

    private final boolean enable;

    public static FieldScope of(int value) {
        for (FieldScope fieldScope : FieldScope.values()) {
            if (fieldScope.value == value) {
                return fieldScope;
            }
        }
        throw new EnumValueException();
    }
}
