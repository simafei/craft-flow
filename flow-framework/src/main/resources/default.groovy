import com.simafei.flow.core.api.ApiResolver

class Default_Resolver implements ApiResolver {

    @Override
    Map<String, String> headers(Map<String, Object> params) {
        // 构造请求头
        return new HashMap<>();
    }

    @Override
    String body(Map<String, Object> params) {
        // 构造请求体JSON
        return "{}";
    }

    @Override
    List<Map<String, Object>> resolve(Map<String, Object> response, Map<String, Object> params) {
        // 请求响应解析
        return List.of(response);
    }

    @Override
    boolean shouldExecute(Map<String, Object> params) {
        // 在这里判断模型需不需要执行
        return true;
    }
}
